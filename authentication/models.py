from django.db import models
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import gettext_lazy as _

from django.core.validators import (
    MaxLengthValidator,
    MinLengthValidator,
    FileExtensionValidator,
    EmailValidator,
    RegexValidator,
)

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE


from authentication.utils import upload_to
from authentication.utils.regex import (
    phone_number,
    characters
)
from authentication.utils.validators.national_code import national_code_validator

from authentication.utils.managers import (
    UserManager,
    UserWiseManager
)

User = settings.AUTH_USER_MODEL
# Create your models here.

class CustomUser(AbstractBaseUser, PermissionsMixin, SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    email = models.EmailField(
        _('Email Address'),
        unique=True,
        validators=[
            EmailValidator
        ]
    )
    phone_number = models.CharField(
        _('Phone Number'),
        max_length=15,
        validators=[
            MinLengthValidator(11, message=_("")),
            MaxLengthValidator(15),
            RegexValidator(phone_number.PERSIAN_PHONE_NUMBER_PATTERN)
        ],
        unique = True
    )

    first_name = models.CharField(
        _('First Name'),
        max_length=30,
        blank=True,
        null=True,
        validators=[
            MinLengthValidator(3),
            MaxLengthValidator(30),
            RegexValidator(
                characters.NATIONAL_CHARS,
                message=_("First Name must have alphabetic characters.")
            ),
        ]
    )

    last_name = models.CharField(
        _('Last Name'),
        max_length=30,
        blank=True,
        null=True,
        validators=[RegexValidator(
            characters.NATIONAL_CHARS,
            message=_("Last Name must have alphabetic characters.")
        )]
    )

    date_joined = models.DateTimeField(
        _('Date Joined'),
         auto_now_add=True
        )

    is_staff = models.BooleanField(
        _('is staff'),
         default=True
        )
    is_active = models.BooleanField(
        _('is active'), 
        default=True
        )

    objects = UserManager()
    wise = UserWiseManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        ordering = ['id']

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def send_sms_user(self, subject, message, from_number=None, **kwargs):
        """
        sends an SMS 
        """
        # send_sms(subject, message, from_number, [self.phone_number], **kwargs)
        pass

    
class StaffUser(CustomUser):
    class Meta:
        proxy = True
