from typing import Any, Dict, Iterable, List
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordContextMixin, SuccessURLAllowedHostsMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import (
    TemplateView,
    DetailView,
    UpdateView,
    ListView
)

from django.utils.translation import gettext_lazy as _

from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

from django.views.generic.edit import FormView

from django.urls import reverse

from django.views.generic.edit import FormMixin

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model

from authentication.forms import (
    CustomAuthenticationForm, 
    CustomPasswordChangeForm, 
    CustomRegisterForm,
    UserForm
)
User = get_user_model()


class AccountView(LoginRequiredMixin, TemplateView):
    page_name = 'user-dashboard'

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        return context

class AccountChangePasswordView(PasswordChangeView):
    form_class = CustomPasswordChangeForm
    success_url = reverse_lazy('account:password_change_done')
    template_name = "pages/user/change_password.html"
    title = _('Password change')
    page_name = 'user-change-password'


class AccountLoginView(LoginView):
    template_name = 'registration/login.html'
    redirect_authenticated_user = True
    form_class = CustomAuthenticationForm
    auth_button_title = _("Log In")


class AccountRegisterView(SuccessURLAllowedHostsMixin, FormView):
    form_class = CustomRegisterForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('account:login')
    auth_button_title = _('Register')

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        # form.send_email()

        form.save()
        return super().form_valid(form)
