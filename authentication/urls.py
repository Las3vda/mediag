from django.urls import path

from django.contrib.auth.views import LogoutView
from authentication.views import (
    AccountView,
    # Auth
    AccountLoginView,
    AccountRegisterView,
    AccountChangePasswordView,
)
app_name = 'account'


urlpatterns = [
    # Account Profile Pages
    path('account/', AccountView.as_view(), name = "user-dashboard"),
    # Account Auth Management
    path('account/login/', AccountLoginView.as_view(), name = "login"),
    path('account/register/', AccountRegisterView.as_view(), name = "register"),
    path('account/change-password/', AccountChangePasswordView.as_view(), name = "user-change-password"),
    path('account/logout/', LogoutView.as_view(), name = "logout"),
    
]