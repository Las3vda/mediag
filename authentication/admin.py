from typing import List, Optional, Tuple, Union
from django.contrib import admin
from django.http.request import HttpRequest
from django.contrib.auth.admin import UserAdmin


from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from safedelete.admin import (
    SafeDeleteAdmin,
    highlight_deleted
)

from authentication.models import (
    StaffUser,
)

CustomUser = get_user_model()


class CustomUserAdmin(UserAdmin, SafeDeleteAdmin):

    list_display = (
        highlight_deleted,
        'phone_number', 
        'email',
        'first_name',
        'last_name',
        'is_staff'
        ) + SafeDeleteAdmin.list_display

    list_filter = (
        'is_staff',
        'is_superuser',
        'is_active',
        'groups'
        ) + SafeDeleteAdmin.list_filter

    search_fields = (
        'phone_number',
        'first_name',
        'last_name',
        'email'
        )

    ordering = ('phone_number',)

    filter_horizontal = (
        'groups',
        'user_permissions',
        )

    readonly_fields = ['date_joined', ]

    fieldsets = (
        (None, {'fields': (
            'email', 'phone_number', 'password'
            )}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'phone_number', 'password1', 'password2'),
        }),
    )

    def get_queryset(self, request):
        group_qs = request.user.groups.all()
        group_names = [group.name for group in group_qs]
        if 'Normal User' in group_names:
            # qs = super().get_queryset(request).filter(email = request.user.email)
            qs = super().get_queryset(request)
        else:
            qs = super().get_queryset(request)

        return qs

    def get_readonly_fields(self, request: HttpRequest, obj: Optional[None] = ...) -> Union[List[str], Tuple]:
        if request.user.is_superuser:
            return super().get_readonly_fields(request, obj=obj)
        else:
            return super().get_readonly_fields(request, obj=obj).extend(
                [
                    'is_staff',
                    'is_superuser',
                    'user_permissions',
                    'groups',
                    'is_active',
                    'last_login'
                ]
            )

    def has_add_permission(self, request: HttpRequest) -> bool:
        if request.user.is_superuser:
            return True
        return False

    def has_change_permission(self, request: HttpRequest, obj: Optional[None] = ...) -> bool:
        # return super().has_change_permission(request, obj=obj)
        if request.user == obj or request.user.is_superuser:
            return True
        return False

    def has_delete_permission(self, request: HttpRequest, obj: Optional[None] = ...) -> bool:
        # return super().has_delete_permission(request, obj=obj)
        if request.user.is_superuser:
            return True
        return False



@admin.register(StaffUser)
class StaffUserAdmin(CustomUserAdmin):

    def get_queryset(self, request):
        return CustomUser.objects.filter(is_staff=True)
    
    def has_add_permission(self, request: HttpRequest) -> bool:
        if request.user.is_superuser:
            return True
        return False

    def has_change_permission(self, request: HttpRequest, obj: Optional[None] = ...) -> bool:
        # return super().has_change_permission(request, obj=obj)
        if request.user == obj or request.user.is_superuser:
            return True
        return False

    def has_delete_permission(self, request: HttpRequest, obj: Optional[None] = ...) -> bool:
        # return super().has_delete_permission(request, obj=obj)
        if request.user.is_superuser:
            return True
        return False

admin.site.register(CustomUser, CustomUserAdmin)
