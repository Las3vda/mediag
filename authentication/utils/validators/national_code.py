from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError


def national_code_validator(value: str) -> bool:
    numbers = [int(num) for num in value[:9]]
    sum, zarib = 0, 10
    try:
        control_num = int(value[9])
    except IndexError:
        raise ValidationError(_('Enter a valid National code !'), params={'value': value})
    for numb in numbers:
        sum += (zarib * numb)
        zarib -= 1
    r = sum % 11
    if r < 2 and r == control_num or r >= 2 and (11 - r) == control_num:
        return True
    else:
        raise ValidationError(_('Enter a valid National code !'), params={'value': value})
