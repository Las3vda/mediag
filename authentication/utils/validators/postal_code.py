from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible

@deconstructible
class PersianPostalCodeValidator(RegexValidator):
    regex = '^[0-9]{10}$',
    message = _('Enter a valid postal code')
