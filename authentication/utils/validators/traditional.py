
def validate_comma_seperator(value):
    if ',' not in value:
        raise ValidationError(
            _('%(value)s is not a comma-separated list.'),
            params={"value": value}
        )