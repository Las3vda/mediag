from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions


@deconstructible
class DimensionValidator(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def __call__(self, value):
        pic = value
        w, h = get_image_dimensions(pic)

        if not (w == self.width and h == self.height):
            raise ValidationError(
                _('Expected Dim: [ %(width)sw , %(height)sh ] But Actual Dim: [ %(w)sw , %(h)sh ].'),
                params={"width": self.width, "height": self.height, "w": w, "h": h}
            )


@deconstructible
class SquareDimension(object):
    def __init__(self, *args, **kwargs):
        pass

    def __call__(self, value):
        pic = value
        w, h = get_image_dimensions(pic)

        if not (w == h):
            raise ValidationError(
                _('Width and Height must be equal (square image).')
            )
