import pytz
from datetime import datetime

from django.db import models
from django.db.models.aggregates import Count
from django.db.models import (
    Sum,
    F,
    Q
)

utc = pytz.UTC


class UserQuerySet(models.QuerySet):

    def filter_by_city(self, city):
        return self.filter(
            addresses__city=city,
            addresses__is_default=True
        )

    def filter_by_state(self, state):
        return self.filter(
            addresses__state=state,
            addresses__is_default=True
        )

    def filter_by_last_login_time(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.filter(
            last_login__lte=utc.localize(end_date),
            last_login__gte=utc.localize(start_date)
        )

    def filter_by_date_joined(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.filter(
            date_joined__lte=utc.localize(end_date),
            date_joined__gte=utc.localize(start_date)
        )

    def filter_by_gender(self, gender):
        return self.filter(profile__gender=gender)

    def filter_orders_by_date(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.filter(
            orders__created__lte=utc.localize(end_date),
            orders__created__gte=utc.localize(start_date)
        )

    def show_count_by_gender(self, gender):
        return self.filter(profile__gender=gender).count()

    def show_gender_percentage(self, gender):
        return (self.filter(profile__gender=gender).count()*100)/(self.count())

    def get_all_genders_percentage(self):
        return self.aggregate(
            male=(Count('profile', filter=Q(profile__gender='m'))
                  * 100) / Count('profile'),
            female=(Count('profile', filter=Q(profile__gender='f'))
                    * 100) / Count('profile'),
            custom=(Count('profile', filter=Q(profile__gender='p'))
                    * 100) / Count('profile'),
        )

    def list_user_transactions_by_email(self, email):
        return self.filter(user__email=email)

    def list_user_transactions_by_phone_number(self, phone_number):
        return self.filter(user__phone_number=phone_number)
