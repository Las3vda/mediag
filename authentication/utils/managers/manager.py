from datetime import datetime

from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.db.models.query import QuerySet

from authentication.utils.querysets.user_quetysets import UserQuerySet


class UserManager(BaseUserManager):
    use_in_migrations = True

    def get_queryset(self):
        return UserQuerySet(self.model, using=self._db)  # Important!

    def _create_user(self, email, phone_number, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        if not phone_number:
            raise ValueError('The given phone number must be set')
        email = self.normalize_email(email)
        user = self.model(
            email=email, phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, phone_number, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, phone_number, password, **extra_fields)

    def create_superuser(self, email, phone_number, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, phone_number, password, **extra_fields)


class UserWiseManager(models.Manager):
    def get_queryset(self):
        return UserQuerySet(self.model, using=self._db)

    def filter_by_city(self, city):
        return self.get_queryset().filter_by_city(city)

    def filter_by_state(self, state):
        return self.get_queryset().filter_by_state(state)

    def filter_by_last_login_time(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.get_queryset().filter_by_last_login_time(start_date, end_date)

    def filter_by_date_joined(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.get_queryset().filter_by_date_joined(start_date, end_date)

    def filter_orders_by_date(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.get_queryset().filter_orders_by_date(start_date, end_date)

    def filter_by_gender(self, gender):
        return self.get_queryset().filter_by_gender(gender)

    def show_count_by_gender(self, gender):
        return self.get_queryset().show_count_by_gender(gender)

    def show_gender_percentage(self, gender):
        return self.get_queryset().show_gender_percentage(gender)

    def list_transactions_by_email(self, email):
        return self.get_queryset().list_user_transactions_by_email(email)

    def list_transactions_by_phone_number(self, phone_number):
        return self.get_queryset().list_user_transactions_by_phone_number(phone_number)
