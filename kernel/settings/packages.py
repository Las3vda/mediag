from datetime import datetime
import os

from .base import (
    INSTALLED_APPS,
    MIDDLEWARE,
    BASE_DIR,
    STATIC_ROOT,
    USE_I18N
)

from django.utils.translation import gettext_lazy as _

from decouple import config


# #################### #
#   ADMIN EXTENTIONS   #
# #################### #
INSTALLED_APPS.append('django.contrib.admindocs')
INSTALLED_APPS.append('django.contrib.humanize')
MIDDLEWARE.append('django.contrib.admindocs.middleware.XViewMiddleware')

INSTALLED_APPS.append('admin_footer')
# INSTALLED_APPS.append('admin_honeypot')
INSTALLED_APPS.append('django_admin_listfilter_dropdown')
INSTALLED_APPS.append('advanced_filters')
INSTALLED_APPS.append('adminsortable2')

# ############## #
#   EXTENSIONS   #
# ############## #

if config("DEBUG_TOOLBAR", default=False, cast=bool):
    INSTALLED_APPS.append('debug_toolbar')

INSTALLED_APPS.append('compressor')
INSTALLED_APPS.append('widget_tweaks')
INSTALLED_APPS.append('sorl.thumbnail')
INSTALLED_APPS.append('mptt')
INSTALLED_APPS.append('safedelete')

INSTALLED_APPS.append('django_extensions')
SHELL_PLUS_IMPORTS = [
    "from django.db import connection as c",
]
# INSTALLED_APPS.append('django.contrib.sitemaps')
# INSTALLED_APPS.append('django.contrib.sites')

# ############## #
# CUSTOM PROJECT #
# ############## #
INSTALLED_APPS.append('authentication')


# ################ #
#    Thumbnail     #
# ################ #
THUMBNAIL_DEBUG = True


# ################ #
#   ADMIN FOOTER   #
# ################ #

ADMIN_FOOTER_DATA = {
    'site_url': config('SITE_BASE_URL'),
    'site_name': config('SITE_NAME'),
    'period': '{}'.format(datetime.now().year),
    'version': 'v0.1.0 - develop '
}

# ######################## #
#   DJANGO DEBUG TOOLBAR   #
# ######################## #
if config("DEBUG_TOOLBAR", default=False, cast=bool):
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
    INTERNAL_IPS = [
        '127.0.0.1',
    ]

# ######################## #
#     STATIC Compressor    #
# ######################## #
if config("COMPRESSOR", default=False, cast=bool):
    COMPRESS_ENABLED = True

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        # other finders..
        'compressor.finders.CompressorFinder',
    )


# ######################## #
#       HTML MINIFIER      #
# ######################## #
if config("HTML_MINIFIER", default=False, cast=bool):
    MIDDLEWARE.extend([
        'htmlmin.middleware.HtmlMinifyMiddleware',
        'htmlmin.middleware.MarkRequestMiddleware',
    ])

    HTML_MINIFY = True
    KEEP_COMMENTS_ON_MINIFYING = False
    CONSERVATIVE_WHITESPACE_ON_MINIFYING = True


# #################### #
#    AUTHENTICATION    #
# #################### #
LOGIN_URL = '/account/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
AUTH_USER_MODEL = 'authentication.CustomUser'


# #################### #
#       Site map       #
# #################### #
SITE_ID = 1

# #################### #
#    SIMPLE HISTORY    #
# #################### #
INSTALLED_APPS.append('simple_history')
MIDDLEWARE.append('simple_history.middleware.HistoryRequestMiddleware')

# ########################### #
#    INTERNATIONALIZATION     #
# ########################### #
MIDDLEWARE.insert(3, 'django.middleware.locale.LocaleMiddleware')

if config('INTERNATIONALIZATION'):
    if not os.path.isdir(os.path.join(BASE_DIR, config('TRANSLATION_DIR'))):
        os.makedirs(os.path.join(BASE_DIR, config('TRANSLATION_DIR')))

# default language
LANGUAGE_CODE = config('DEFAULT_LANGUAGE')

# supported languages
LANGUAGES = (
    ("en", _("English")),
    ("fa", _("Persian")),
    ("ar", _("Arabic")),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, config('TRANSLATION_DIR')),
)
