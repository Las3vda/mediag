from django.contrib import admin
from django.utils.safestring import mark_safe

from simple_history.admin import SimpleHistoryAdmin
from mptt.admin import (
    DraggableMPTTAdmin,
    TreeRelatedFieldListFilter,
)
from safedelete.admin import (
    SafeDeleteAdmin,
    highlight_deleted
)

from warehouse.utils.actions.traditional import (
    activate,
    deactivate,
    make_special,
    make_unspecial
)

from warehouse.models import (
    Product,
    Category,
    Brand,
    Color,
    Pack,
    Guarantee,
)


class PackAdminInline(admin.StackedInline):
    model = Pack
    

class CategoryDraggableMPTTAdmin(DraggableMPTTAdmin):
    MPTT_ADMIN_LEVEL_INDENT = 20
    list_display_links = ('title',)
    mptt_indent_field = "title"


@admin.register(Product)
class ProductAdmin(SimpleHistoryAdmin, SafeDeleteAdmin):
    list_display = (
        highlight_deleted,
        'display_status',
        'display_title',
        'is_active',
        'is_special',
        'in_stock',
        'display_created',
        'display_modified',
        'modified_by',
    ) + SafeDeleteAdmin.list_display
    list_filter = (
        'is_active',
        'is_special',
        'created',
        'modified'
    ) + SafeDeleteAdmin.list_filter
    search_fields = ['sku', 'title', 'summary']
    history_list_display = ["in_stock", 'is_active', 'is_special']
    save_on_top = True
    empty_value_display = '-N/A-'
    list_editable = ['in_stock']
    readonly_fields = ['sku', 'slug', 'created', 'modified', 'modified_by']
    raw_id_fields = ['category']
    inlines = [
        PackAdminInline
    ]
    actions = [
        activate,
        deactivate,
        make_special,
        make_unspecial
    ]
    fieldsets = (
        ('Basic Information', {
            'fields': (
                'sku',
                'title',
                'slug',
                'category'
            )
        }),
        ('Product Information', {
            'classes': ('collapse',),
            'fields': (
                'picture',
                ('summary', 'subtitle'),
                'description',
                ('default_rating', 'show_default_rating'),
            ),
        }),
        ('Product Availability', {
            'classes': ('collapse',),
            'fields': (
                'is_active',
                'is_special',
                'in_stock'
            ),
        }),
        ('Security Center', {
            'classes': ('collapse',),
            'fields': (
                'created',
                'modified',
                'modified_by'
            ),
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)

    @admin.display(description='title')
    def display_title(self, obj):
        return (obj.title[:50] + '..') if len(obj.title) > 50 else obj.title

    @admin.display(description='status')
    def display_status(self, obj):
        parameters = ('#f46a6a', 'Out of stock',) if obj.in_stock == 0 else (
            '#1e9368', 'Available',)
        data_point = '<span style="color: {};">{}</span>'.format(*parameters)
        return mark_safe(data_point)

    @admin.display(description='created', ordering='created')
    def display_created(self, obj):
        return obj.created.strftime('%Y-%m-%d')

    @admin.display(description='modified', ordering='modified')
    def display_modified(self, obj):
        return obj.modified.strftime('%Y-%m-%d')


@admin.register(Category)
class CategoryAdmin(SimpleHistoryAdmin, SafeDeleteAdmin, CategoryDraggableMPTTAdmin, TreeRelatedFieldListFilter):

    list_display = (
        'tree_actions',
        highlight_deleted,
        'title',
        'display_parent',
        'is_active',
        'created',
        'modified',
        'modified_by'
    ) + SafeDeleteAdmin.list_display

    list_filter = (
        'is_active',
        'created',
        'modified',
        'modified_by',
        ('parent', TreeRelatedFieldListFilter)
    ) + SafeDeleteAdmin.list_filter
    search_fields = ['title']
    # history_list_display = ['title', 'is_active', 'parent']
    save_on_top = True
    empty_value_display = '-NA-'
    readonly_fields = ['slug', 'created', 'modified', 'modified_by']

    def save_model(self, request, obj, form, change):
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)

    @admin.display(description='parent')
    def display_parent(self, obj):
        parameters = ('#f46a6a', 'MAIN',) if obj.parent is None else (
            'fff', obj.parent,)
        data_point = '<span style="color: {};">{}</span>'.format(*parameters)
        return mark_safe(data_point)

    def get_queryset(self, request):
        qs = Category.objects.all().select_related('parent', 'modified_by')
        return qs


@admin.register(Brand)
class BrandAdmin(SimpleHistoryAdmin, SafeDeleteAdmin):
    list_display = (highlight_deleted, 'title') + SafeDeleteAdmin.list_display
    list_filter = SafeDeleteAdmin.list_filter
    search_fields = ['title']
    history_list_display = ['title']
    readonly_fields = ['slug', 'created', 'modified', 'modified_by']
    save_on_top = True
    fieldsets = (
        ('Basic Information', {
            'fields': ('title', 'slug',)
        }),
        ('Security Center', {
            'classes': ('collapse',),
            'fields': (
                'created',
                'modified',
                'modified_by'
            ),
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Guarantee)
class GuaranteeAdmin(SimpleHistoryAdmin, SafeDeleteAdmin):
    list_display = (highlight_deleted, 'title_fa', 'title_en') + \
        SafeDeleteAdmin.list_display
    list_filter = SafeDeleteAdmin.list_filter
    search_fields = ['title_en', 'title_fa']
    history_list_display = ['title_fa']
    save_on_top = True
    list_editable = ['title_en']
    readonly_fields = ['created', 'modified', 'modified_by']
    fieldsets = (
        ('Basic Information', {
            'fields': ('title_fa', 'title_en',)
        }),
        ('Security Center', {
            'classes': ('collapse',),
            'fields': (
                'created',
                'modified',
                'modified_by'
            ),
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Color)
class ColorAdmin(SimpleHistoryAdmin, SafeDeleteAdmin):
    list_display = (highlight_deleted, 'title_fa', 'hex_code') + \
        SafeDeleteAdmin.list_display
    list_filter = SafeDeleteAdmin.list_filter
    search_fields = ['title_fa', 'title_en', 'hex_code']
    history_list_display = ['title_fa']
    save_on_top = True
    readonly_fields = ['created', 'modified', 'modified_by']
    list_editable = ['hex_code']
    fieldsets = (
        ('Basic Information', {
            'fields': ('title_fa', 'title_en', 'hex_code')
        }),
        ('Security Center', {
            'classes': ('collapse',),
            'fields': (
                'created',
                'modified',
                'modified_by'
            ),
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Pack)
class PackAdmin(SimpleHistoryAdmin, SafeDeleteAdmin):
    list_display = (
        highlight_deleted,
        'price',
        'buy_price',
        'is_active',
        'is_default',
        'is_supply',
        'in_stock',
        'guarantee',
        'product',
        'color',
        'created',
        'modified',
        'modified_by'
    ) + SafeDeleteAdmin.list_display
    list_filter = (
        'is_active',
        'is_default',
        'created',
        'modified'
    ) + SafeDeleteAdmin.list_filter
    search_fields = ['price', 'color', 'guarantee', 'product']
    history_list_display = ['price']
    save_on_top = True
    readonly_fields = ['sku', 'created', 'modified', 'modified_by']
    # list_editable = ['is_active']
    fieldsets = (
        ('Basic Information', {
            'fields': ('is_active', 'is_default', 'in_stock', 'is_supply')
        }),
        ('Message Information', {
            'classes': ('collapse',),
            'fields': (
                'price',
                'guarantee',
                'product',
                'color',
            )
        }),
        ('Security Center', {
            'classes': ('collapse',),
            'fields': (
                'created',
                'modified',
                'modified_by'
            ),
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)
