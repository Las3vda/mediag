from warehouse.models.category import Category
from warehouse.models.product import Product
from warehouse.models.pack import Pack

from warehouse.models.color import Color
from warehouse.models.guarantee import Guarantee
from warehouse.models.brand import Brand

from simple_history import register

register(Category)
register(Product)
register(Pack)
