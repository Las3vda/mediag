import random

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify
from django.urls import reverse
from django.core.validators import MinValueValidator

from sorl.thumbnail import get_thumbnail
from sorl.thumbnail import delete
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from painless.models import (
    SKUMixin,
    TitleSlugDescriptionMixin,
    TrackingModifierMixin,
    ImageDateBasedMixin
)
from warehouse.utils.managers import (
    ProductManager,
)


class Product(SKUMixin, TitleSlugDescriptionMixin, TrackingModifierMixin, ImageDateBasedMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    subtitle = models.CharField(
        _("Sub Title"),
        max_length=150,
        help_text=_("Sub Title of each product"),
        unique=True
    )

    summary = models.CharField(
        _("Summary"),
        max_length=200,
        help_text=_("Short Summary of each product")
    )

    is_active = models.BooleanField(
        _("Is Active"),
        help_text=_("Show the product availability on website."),
        default=False
    )

    is_special = models.BooleanField(
        _("Is Special"),
        help_text=_("Special tag icon on product list."),
        default=False
    )

    in_stock = models.PositiveIntegerField(
        _("In Stock"),
        help_text=_("In Stock quantity of products."),
        default=0,
        validators=[
            MinValueValidator(0)
        ]
    )

    show_default_rating = models.BooleanField(
        _("Show Default Rating"),
        help_text=_("Show cold start products from `default_rating` field."),
        default=True
    )

    default_rating = models.FloatField(
        _("Default Rating"),
        help_text=_("A default rating for cold start products"),
        default=0.0
    )

    category = models.ForeignKey(
        'Category',
        verbose_name=_("Category"),
        related_name="products",
        on_delete=models.CASCADE
    )

    brand = models.ForeignKey(
        'Brand',
        verbose_name=_("Brand"),
        related_name="products",
        on_delete=models.PROTECT
    )

    objects = models.Manager()
    wise = ProductManager()

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        ordering = ['id']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        sku = "".join(random.sample("123456789"*3, k=7))
        if not self.sku:
            self.sku = "SWP-{}".format(sku)
        super().save(*args, **kwargs)

    @property
    def get_default_pic_url(self):
        if self.picture:
            return self.picture.url
        return ''

    @property
    def thumbnail(self):
        return get_thumbnail(self.picture, '100x100', crop='center', quality=99)

    @property
    def is_out_of_stock(self):
        if self.in_stock == 0:
            return True
        return False

    @property
    def list_available_colors(self):
        return self.packs(manager="wise").in_stock().values("color")

    @property
    def list_unavailable_colors(self):
        return self.packs(manager="wise").out_of_stock().values("color")

    @property
    def list_available_guarantee(self):
        return self.packs(manager="wise").in_stock().values("guarantee")

    @property
    def get_default_pack(self):
        return self.packs.filter(is_default=True)

    @property
    def get_packs(self):
        return self.packs.all()

    @property
    def get_packs_count(self):
        return self.packs.count()

    @property
    def get_active_packs(self):
        return self.packs(manager="wise").filter(is_active=True)

    @property
    def get_deactive_packs(self):
        return self.packs(manager="wise").filter(is_active=False)

    @property
    def get_review_count(self):
        return self.reviews.count()

    def get_absolute_url(self):
        return reverse("pages:product-detail", kwargs={"sku": self.sku, "slug": self.slug})

    def __str__(self):
        return self.sku

    def __repr__(self):
        return self.sku
