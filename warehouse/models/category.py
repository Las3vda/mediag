from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify
from django.urls import reverse

from mptt.models import MPTTModel, TreeForeignKey
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from painless.models import (
    TitleSlugMixin,
    TrackingModifierMixin,
)
from warehouse.utils.managers import CategoryManager


class Category(MPTTModel, TitleSlugMixin, TrackingModifierMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE_CASCADE

    is_active = models.BooleanField(
        _("is Active"),
        help_text=_("is Shown in website"),
        default=True
    )

    parent = TreeForeignKey(
        'self',
        verbose_name=_("Parent Category"),
        related_name="Children",
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )
    # objects = models.Manager()
    wise = CategoryManager()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.slug

    def __repr__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse("pages:category-detail", kwargs={"slug": self.slug})

    class MPTTMeta:
        order_insertion_by = ['title']

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    @property
    def get_in_stock_products(self):
        return self.products(manager="wise").in_stock().all()

    @property
    def get_out_of_stock_products(self):
        return self.products(manager="wise").out_of_stock().all()

    @property
    def get_products(self):
        return self.products.all()

    @property
    def get_products_count(self):
        return self.products.count()

    @property
    def get_active_products(self):
        return self.products(manager="wise").actives().all()

    @property
    def get_deactive_products(self):
        return self.products(manager="wise").deactives().all()

    @property
    def get_specials_products(self):
        return self.products(manager="wise").specials().all()
