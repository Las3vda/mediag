from django.db import models
from django.utils.translation import gettext_lazy as _

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from painless.models import TrackingModifierMixin


class Color(TrackingModifierMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE_CASCADE

    title_en = models.CharField(
        _("Title en"),
        max_length=30,
        help_text=_("Name of this color (English)"),
        unique=True
    )

    title_fa = models.CharField(
        _("Title fa"),
        max_length=30,
        help_text=_("Name of this color (Farsi)"),
        unique=True
    )

    hex_code = models.CharField(
        _("Hex Code"),
        max_length=10,
        help_text=_("Hex cod of this color"),
        unique=True,
    )

    def __str__(self):
        return self.title_en

    def __repr__(self):
        return self.title_en
