from django.db import models
from django.utils.translation import gettext_lazy as _

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from painless.models import (
    TrackingModifierMixin,

)


class Guarantee(TrackingModifierMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE_CASCADE

    title_en = models.CharField(
        _("Title en"),
        max_length=150,
        help_text=_("Sub guarantee en Title of each product"),
        unique=True
    )

    title_fa = models.CharField(
        _("Title fa"),
        max_length=150,
        help_text=_("Sub guarantee fa Title of each product"),
        unique=True
    )
