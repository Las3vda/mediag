from django.db import models
from django.utils.translation import gettext_lazy as _

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from painless.models import (
    TitleSlugMixin,
    TrackingModifierMixin,
)
from warehouse.utils.managers import (
    BrandManager,
)


class Brand(TitleSlugMixin, TrackingModifierMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE_CASCADE

    wise = BrandManager()
    objects = models.Manager()

    def __repr__(self) -> str:
        return self.title

    def __str__(self) -> str:
        return self.title

    @property
    def get_in_stock_products(self):
        return self.products(manager="wise").in_stock().all()

    @property
    def get_out_of_stock_products(self):
        return self.products(manager="wise").out_of_stock().all()

    @property
    def get_products(self):
        return self.products.all()

    @property
    def get_products_count(self):
        return self.products.count()

    @property
    def get_active_products(self):
        return self.products(manager="wise").actives().all()

    @property
    def get_deactive_products(self):
        return self.products(manager="wise").deactives().all()

    @property
    def get_specials_products(self):
        return self.products(manager="wise").specials().all()

    def __str__(self):
        return self.slug

    def __repr__(self):
        return self.slug
