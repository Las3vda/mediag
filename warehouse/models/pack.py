import random

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify
from django.core.validators import MinValueValidator

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from warehouse.utils.managers.pack_manager import PackManager
from painless.models import (
    SKUMixin,
    TrackingModifierMixin,
)


class Pack(SKUMixin, TrackingModifierMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    price = models.PositiveBigIntegerField(
        _("Sell Price"),
        help_text=_("The price of selling this Pack."),
        validators=[
            MinValueValidator(10_000)
        ]
    )

    buy_price = models.PositiveBigIntegerField(
        _("Buy Price"),
        help_text=_("The price of buying this Pack."),
        validators=[
            MinValueValidator(10_000)
        ]
    )

    is_active = models.BooleanField(
        _("Is Active"),
        help_text=_("Show the product availability on website."),
        default=False
    )

    is_default = models.BooleanField(
        _("Is Default"),
        help_text=_("Show this pack as default pack."),
        default=False
    )

    actual_in_stock = models.PositiveIntegerField(
        _("Actual In Stock"),
        help_text=_("Actual In Stock quantity of products available."),
        default=0,
        validators=[
            MinValueValidator(0)
        ]
    )


    in_stock = models.PositiveIntegerField(
        _("In Stock"),
        help_text=_("In Stock quantity of products to show users."),
        default=0,
        validators=[
            MinValueValidator(0)
        ]
    )

    is_supply = models.BooleanField(
        _("IS SUPPLY"),
        help_text=_("In Stock quantity of products."),
        default=False
    )

    product = models.ForeignKey(
        'Product',
        verbose_name=_("Product"),
        related_name='packs',
        on_delete=models.CASCADE
    )

    guarantee = models.ForeignKey(
        'Guarantee',
        verbose_name=_("Guarantee"),
        related_name="packs",
        on_delete=models.CASCADE
    )

    color = models.ForeignKey(
        'Color',
        verbose_name=_("Color"),
        related_name="packs",
        on_delete=models.CASCADE
    )

    objects = models.Manager()
    wise = PackManager()

    def save(self, *args, **kwargs):
        sku = "".join(random.sample("123456789"*3, k=7))
        if not self.sku:
            self.sku = "wise-{}".format(sku)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.sku

    def __repr__(self):
        return self.sku
