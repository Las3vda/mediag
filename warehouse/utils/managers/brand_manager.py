from django.db import models
from warehouse.utils.querysets.brand_queryset import BrandQueryset
from datetime import datetime
import pytz
utc = pytz.UTC


class BrandManager(models.Manager):

    def get_queryset(self):
        return BrandQueryset(self.model, using=self._db)  # Important!

    def filter_by_title(self, title):
        return self.get_queryset().filter_by_title(title)

    def filter_order_by_city(self, city):
        return self.get_queryset().filter_order_by_city(city)

    def filter_wishlist_by_city(self, city):
        return self.get_queryset().filter_wishlist_by_city(city)

    def filter_order_by_date(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.get_queryset().filter_order_by_date(start_date, end_date)

    def filter_order_by_statuses(self, statuses):
        return self.get_queryset().filter_order_by_statuses(statuses)

    def get_sold_count(self):
        return self.get_queryset().get_sold_count()

    def get_carted_count(self):
        return self.get_queryset().get_carted_count()

    def get_wished_count(self):
        return self.get_queryset().get_wished_count()

    def get_user_bought_count(self, distinct=False):
        return self.get_queryset().get_bought_users_count(distinct)

    def get_bought_users(self, distinct=False):
        return self.get_queryset().get_bought_users(distinct)

    def filter_by_action_and_city_and_behavior_by_date(self, action=["fv", "l", "dl"], value=True, start_date=datetime(2000, 1, 1), end_date=datetime.now(), city="-",):
        return self.get_queryset().filter_by_action_and_city_and_behavior_by_date(action, value, start_date, end_date, city)

    def get_all_products_count_by_title(self, title):
        return self.get(title=title).products.count()

    def get_products_by_title(self, title):
        return self.get(title=title).products.all()
