from django.db import models
from warehouse.utils.querysets import PackQueryset
from datetime import datetime
import pytz
utc = pytz.UTC


class PackManager(models.Manager):

    def get_queryset(self):
        return PackQueryset(self.model, using=self._db)  # Important!

    def in_stock(self):
        return self.get_queryset().in_stock()

    def out_of_stock(self):
        return self.get_queryset().out_of_stock()

    def range_stock(self, level='low'):
        return self.get_queryset().range_stock(level)

    def actives(self):
        return self.get_queryset().actives()

    def deactives(self):
        return self.get_queryset().deactives()

    def in_stock(self):
        return self.get_queryset().in_stock()

    def out_of_stock(self):
        return self.get_queryset().out_of_stock()

    def filter_orders_by_date(self, start_date=datetime(2000, 1, 1), end_date=datetime.now()):
        return self.get_queryset().filter_orders_by_date(start_date, end_date)

    def filter_by_sku(self, sku):
        return self.get_queryset().filter_by_sku(sku)

    def filter_by_guarantee(self, title_en):
        return self.get_queryset().filter_by_guarantee(title_en)

    def filter_by_color(self, title_en):
        return self.get_queryset().filter_by_color(title_en)

    def get_sold_count(self):
        return self.get_queryset().get_sold_quantity()

    def get_sold_price(self):
        return self.get_queryset().get_sold_price()

    def filter_by_category(self, title):
        return self.get_queryset().filter_by_category(title)

    def get_most_sold_by_count(self):
        return self.get_queryset().get_most_sold_by_count()

    def get_most_sold_by_price(self):
        return self.get_queryset().get_most_sold_by_price()
