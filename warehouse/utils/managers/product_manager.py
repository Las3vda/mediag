import pytz
from datetime import datetime

from django.db import models
from django.db.models.aggregates import Count

from warehouse.utils.querysets import ProductQueryset
from warehouse.models import pack

from safedelete.managers import SafeDeleteManager

utc = pytz.UTC


class ProductManager(SafeDeleteManager):

    def get_queryset(self):
        return ProductQueryset(self.model, using=self._db)  # Important!

    def in_stock(self):
        return self.get_queryset().in_stock()

    def list_all_ratings(self):
        return self.get_queryset().list_all_ratings()

    def out_of_stock(self):
        return self.get_queryset().out_of_stock()

    def range_stock(self, level='low'):
        return self.get_queryset().range_stock(level)

    def actives(self):
        return self.get_queryset().actives()

    def deactives(self):
        return self.get_queryset().deactives()

    def specials(self):
        return self.get_queryset().specials()

    def get_avg_reviews_score(self):
        return self.get_queryset().get_avg_reviews_score()

    def get_min_review_score(self):
        return self.get_queryset().get_min_review_score()

    def get_max_review_score(self):
        return self.get_queryset().get_max_review_score()

    def get_max_sold_pack(self):
        return self.get_queryset().get_max_sold_pack()

    def get_min_sold_pack(self):
        return self.get_queryset().get_min_sold_pack()

    def has_default_rating(self):
        return self.get_queryset().has_default_rating()

    def filter_orders_by_date(self, start_date=datetime(2000, 1, 1), end_date=datetime.now()):
        return self.get_queryset().filter_orders_by_date(start_date, end_date,)

    def filter_orders_by_city(self, city):
        return self.get_queryset().filter_orders_by_city(city)

    def filter_by_sku(self, sku):
        return self.get_queryset().filter_by_sku(sku)

    def filter_by_price_range(self, min_price=0, max_price=100000000):
        return self.get_queryset().filter_by_price_range(min_price, max_price)

    def get_related_orders(self):
        return self.get_queryset().get_related_orders()

    def get_related_brand(self):
        return self.get_queryset().get_related_brand()

    def get_related_category(self):
        return self.get_queryset().get_related_category()

    def get_sold_quantity(self):
        return self.get_queryset().get_sold_quantity()

    def get_recommended_count(self):
        return self.get_queryset().get_recommended_count()

    def get_sold_price(self):
        return self.get_queryset().get_sold_price()

    def get_user_bought_count(self, distinct=False):
        # distinct check nashod-_-
        return self.get_queryset().get_user_bought_count(distinct)

    def list_all_ratings(self):
        return self.get_queryset().list_all_ratings()

    def get_wishlisted_count(self):
        return self.get_queryset().get_wishlisted_count()

    def get_bought_users(self, distinct=False):
        return self.get_queryset().get_bought_users(distinct)

    def filter_by_action_and_city_and_behavior_by_date(self, action=["fv", "l", "dl"], value=True, city="-", start_date=datetime(2000, 1, 1), end_date=datetime.now()):
        return self.get_queryset().filter_by_action_and_city_and_behavior_by_date(action, value, start_date, end_date, city)
    
    def get_products_with_pack_color(self):
        return self.get_queryset().get_products_with_pack_color()

    # def filter_by_action_and_city_and_behavior_by_date(self, action=["fv", "l", "dl"], value=True, city=None, start_date=datetime(2000, 1, 1), end_date=datetime.now()):
    #     from behavior.models import Behavior
    #     products_id = list(self.get_queryset().values_list("id", flat=True))
    #     print(products_id)
    #     if city:
    #         return Behavior.objects.filter(
    #             scope=("p"),
    #             value=value,
    #             action__in=action,
    #             object_id__in=products_id,
    #             created__gte=utc.localize(start_date),
    #             created__lte=utc.localize(end_date),
    #         ).count()
    #     else:
    #         return Behavior.objects.filter(
    #             scope=("p"),
    #             value=value,
    #             action__in=action,
    #             object_id__in=products_id,
    #             created__gte=utc.localize(start_date),
    #             created__lte=utc.localize(end_date),
    #             user__addresses__city=city, user__addresses__is_default=True

    #         ).count()

    # def filter_behavior_by_action(self, action):
    #     return self.get_queryset().filter_behavior_by_action(action)

    # def filter_behavior_by_city(self, city):
    #     return self.get_queryset().filter_behavior_by_city(city)

    # def filter_behavior_by_action(self, action="fv"):
    #     from behavior.models import Behavior
    #     products_id = list(self.values_list("id", flat=True))
    #     return Behavior.objects.filter(
    #         scope=("p"),
    #         value=True,
    #         action=action,
    #         object_id__in=products_id
    #     )

    # def filter_behavior_by_city(self, city):
    #     from behavior.models import Behavior
    #     products_id = list(self.values_list("id", flat=True))
    #     return Behavior.objects.filter(scope=("p"), value=True, object_id__in=products_id,
    # user__addresses__city=city, user__addresses__is_default=True)
# be manager borde shavad
