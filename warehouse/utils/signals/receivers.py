from django.dispatch import receiver
from warehouse.models import Product,Pack
from django.db.models.signals import post_save,pre_save
from django.contrib.auth import get_user_model
from warehouse.utils.signals.signals import *
from django.core.mail import send_mail
User=get_user_model()

#receiver actions

@receiver(send_mail_done)
def send_mail_rec(instance,sender,*args,**kwargs):
    if instance==User:
        message="Product is updated"
        send_mail(message,[instance.EMAIL],sender,*args,**kwargs)
post_save.connect(send_mail_rec,sender=Product)

@receiver(product_added)
def product_added_rec(instance,sender,created,*args,**kwargs):
    if created:
        instance=Product.objects.create(Product=instance)
        instance.save()
        print("Product added successfully",instance,sender,*args,**kwargs)
product_added.connect(product_added_rec,sender=Product)

@receiver(product_removed)
def product_removed_rec(instance,sender,deleted,*args,**kwargs):
    if deleted:
        instance=Product.objects.delete(Product=instance)
        instance.save()
        print("Product removed successfully",instance,sender,*args,**kwargs)
product_removed.connect(product_removed_rec,sender=Product)

@receiver(product_updated)
def product_updated_rec(instance,sender,updated,*args,**kwargs):
    if updated:
        instance=Product.objects.update(Product=instance)
        instance.save()
        print("Product updated successfully",instance.sku,sender,**kwargs)
product_updated.connect(product_updated_rec,sender=Product)

#PACK
@receiver(pack_added)
def pack_added_rec(instance,sender,created,*args,**kwargs):
    if created:
        instance=Pack.objects.create(Pack=instance)
        instance.save()
        print("ADDED successfully",instance,sender,*args,**kwargs)
pack_added.connect(pack_added_rec,sender=Pack)

@receiver(pack_removed)
def pack_removed_rec(instance,sender,deleted,*args,**kwargs):
    if deleted:
        instance=Pack.objects.delete(Pack=instance)
        instance.save()
        print("REMOVRD successfully", instance,sender,*args,**kwargs)
pack_removed.connect(pack_removed_rec,sender=Pack)

@receiver(pack_updated)
def pack_updated_rec(instance,sender,updated,*args,**kwargs):
    if updated:
        instance=Product.objects.update(Pack=instance)
        instance.save()
        print("Product updated successfully",instance.sku,sender,*args,**kwargs)
pack_updated.connect(pack_updated_rec,sender=Pack)
