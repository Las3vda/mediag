from warehouse.models.pack import Pack
from warehouse.utils.signals import signals
from warehouse.models import Product 

#senders
def send_mail_task():
    signals.send_mail_done.send(sender=Product,instance="instance")

def product_added_task():
    signals.product_added.send(sender=Product,instance="instance")

def product_removed_task():
    signals.product_removed.send(sender=Product,instance="instance") 

def product_updated_task():
    signals.product_updated.send(sender=Product,instance="instance") 


def pack_added_task():
    signals.product_added.send(sender=Pack,instance="instance")

def pack_removed_task():
    signals.product_removed.send(sender=Pack,instance="instance") 

def pack_updated_task():
    signals.product_removed.send(sender=Pack,instance="instance") 

