from django.dispatch import Signal
#signals
send_mail_done=Signal(providing_args=["instance"])
product_added=Signal(providing_args=["instance"])
product_removed=Signal(providing_args=["instance"])
product_updated=Signal(providing_args=["instance"])
pack_added=Signal(providing_args=["instance"])
pack_removed=Signal(providing_args=["instance"])
pack_updated=Signal(providing_args=["instance"])