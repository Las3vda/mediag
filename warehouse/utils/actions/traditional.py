from django.contrib import admin

@admin.action(description="Mark selected as activated")
def activate(modeladmin, request, queryset):
    queryset.update(is_active = True)

@admin.action(description="Mark selected as de-activated")
def deactivate(modeladmin, request, queryset):
    queryset.update(is_active = False)

@admin.action(description="Mark selected as special")
def make_special(modeladmin, request, queryset):
    queryset.update(is_special = True)

@admin.action(description="Mark selected as un-special")
def make_unspecial(modeladmin, request, queryset):
    queryset.update(is_special = False)