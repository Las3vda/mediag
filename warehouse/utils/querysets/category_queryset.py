from django.db.models import (
    Count,
    Sum,
    query,

)
from django.db import models
from datetime import datetime
from django.db.models.query_utils import Q
import pytz
utc = pytz.UTC


class CategoryQueryset(models.QuerySet):
    def actives(self):
        return self.filter(is_active=True)

    def deactives(self):
        return self.filter(is_active=False)

    def filter_by_title(self, title):
        return self.filter(title=title)

    def filter_order_by_date(self, start_date=datetime(2000, 1, 1), end_date=(datetime.now())):
        return self.filter(products__packs__order_packs__order__created__lte=utc.localize(end_date), products__packs__order_packs__order__created__gte=utc.localize(start_date))

    def filter_order_by_city(self, city):  # remain test
        return self.filter(products__packs__order_packs__order__city=city)

    def filter_wishlist_by_city(self, city):  # remain test
        return self.filter(products__packs__wishlist_packs__wishlist__user__addresses__city=city,
                           products__packs__wishlist_packs__wishlist__user__addresses__is_default=True)

    def filter_order_by_statuses(self, statuses):
        return self.filter(products__packs__order_packs__order__status__in=statuses)

    def get_user_bought_count(self, distinct):

        return self.annotate(bought=Count("products__packs__order_packs__order__user", distinct=distinct))

    def get_bought_users(self, distinct=False):
        if distinct:
            return self.values("products__packs__order_packs__order__user").distinct()

        return self.values("products__packs__order_packs__order__user")

    def get_sold_count(self):
        return self.annotate(sold=Sum("products__packs__order_packs__quantity"))

    def get_carted_count(self):  # remain test
        return self.annotate(carted=Sum("products__packs__cart_packs__quantity"))

    def get_wished_count(self):  # remain test
        return self.annotate(wished=Sum("products__packs__wishlist_packs__quantity"))

    # remain test
    def filter_by_action_and_city_and_behavior_by_date(self, action=["fv", "l", "dl"], value=True, start_date=datetime(2000, 1, 1), end_date=datetime.now(), city="-",):
        from behavior.models import Behavior
        products_id = list(self.values_list("products__id", flat=True))
        if city == "-":
            return Behavior.objects.filter(
                scope=("p"),
                value=value,
                action__in=action,
                object_id__in=products_id,
                created__gte=utc.localize(start_date),
                created__lte=utc.localize(end_date)
            ).count()
        else:
            return Behavior.objects.filter(
                scope=("p"),
                value=value,
                action__in=action,
                object_id__in=products_id,
                created__gte=utc.localize(start_date),
                created__lte=utc.localize(end_date),
                user__addresses__city=city,
                user__addresses__is_default=True

            ).count()
