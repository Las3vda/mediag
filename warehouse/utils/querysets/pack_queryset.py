from django.db import models
from django.db.models import (
    Sum,
    F,
    Max
)
from datetime import datetime
import pytz
utc = pytz.UTC


class PackQueryset(models.QuerySet):
    def range_stock(self, level='low'):
        if level == 'empty':
            qs = self.filter(in_stock=0)
        elif level == 'low':
            qs = self.filter(in_stock_gte=1, in_stock__lt=100)
        elif level == 'medium':
            qs = self.filter(in_stock__gte=100, in_stock__lt=500)
        elif level == 'huge':
            qs = self.filter(in_stock__gte=500)
        else:
            raise Exception("Product Queryset: invalid param")
        return qs

    def actives(self):
        return self.filter(is_active=True)

    def deactives(self):
        return self.filter(is_active=False)

    def in_stock(self):
        return self.filter(in_stock__gt=0)

    def out_of_stock(self):
        return self.filter(in_stock=0)

    def filter_orders_by_date(self, start_date=datetime(2000, 1, 1), end_date=datetime.now()):
        return self.filter(orders__created__lte=utc.localize(end_date),
                           orders__created__gte=utc.localize(start_date))

    def filter_by_sku(self, sku):  # not checked
        return self.filter(product__sku=sku)

    def filter_by_guarantee(self, title):
        return self.filter(guarantee__title_en=title)

    def filter_by_color(self, title):
        return self.filter(color__title_en=title)

    def filter_by_category(self, title):
        return self.filter(product__category__title=title)

    def get_sold_quantity(self):
        qs = self.annotate(sold_count=Sum("order_packs__quantity"))
        return qs

    def get_sold_price(self):
        qs = self.annotate(
            sold_price=Sum(
                F("order_packs__quantity") * F("order_packs__price")))
        return qs

    def get_most_sold_by_count(self):
        qs = self.get_sold_quantity()
        most = qs.aggregate(most=Max("sold_count"))["most"]
        most_sold_pack = qs.get(sold_count=most)

        return most_sold_pack

    def get_most_sold_by_price(self):
        qs = self.get_sold_price()
        most = qs.aggregate(most=Max("sold_price"))["most"]
        most_sold_pack = qs.get(sold_price=most)

        return most_sold_pack
