import pytz
from datetime import datetime

from django.db import models
from django.db.models import (
    Sum,
    F
)

from django.apps import apps
from django.db.models.aggregates import Avg, Count, Max, Min
from django.db.models.query import Prefetch
from django.db.models.query_utils import Q

utc = pytz.UTC


class ProductQueryset(models.QuerySet):
    def range_stock(self, level='low'):
        if level == 'empty':
            qs = self.filter(in_stock=0)
        elif level == 'low':
            qs = self.filter(in_stock__gte=1, in_stock__lt=100)
        elif level == 'medium':
            qs = self.filter(in_stock__gte=100, in_stock__lt=500)
        elif level == 'huge':
            qs = self.filter(in_stock__gte=500)
        else:
            raise Exception("Product Queryset: invalid param")
        return qs

    def actives(self):
        return self.filter(is_active=True)

    def deactives(self):
        return self.filter(is_active=False)

    def in_stock(self):
        return self.filter(in_stock__gt=0)

    def out_of_stock(self):
        return self.filter(in_stock=0)

    def specials(self):
        return self.filter(is_special=True)

    def has_default_rating(self):
        return self.filter(show_default_rating=True)

    def filter_orders_by_date(self, start_date=datetime(2000, 1, 1), end_date=datetime.now()):
        return self.filter(
            packs__order_packs__order__created__lte=utc.localize(end_date),
            packs__order_packs__order__created__gte=utc.localize(start_date))

    def filter_orders_by_city(self, city):
        return self.filter(packs__order_packs__order__city=city)

    def filter_by_sku(self, sku):
        return self.filter(sku=sku)

    def filter_by_price_range(self, min_price=0, max_price=100000000):
        return self.filter(packs__price__range=[min_price, max_price]).distinct()

    def get_related_orders(self):
        #not checked
        return self.prefetch_related("packs__order_packs__order")

    def get_related_brand(self):
        return self.select_related('brand')

    def get_related_category(self):
        return self.select_related('category')

    def get_min_review_score(self):  # remain_test
        qs = self.annotate(min_score=Min("reviews__rating"))
        return qs

    def get_max_review_score(self):  # remain_test
        qs = self.annotate(max_score=Max("reviews__rating"))
        return qs

    def get_avg_reviews_score(self):  # remain_test
        qs = self.annotate(avg_score=Avg("reviews__rating"))
        return qs

    def list_all_ratings(self):  # remain_test
        qs = self.annotate(
            score_1=Count('reviews', filter=Q(reviews__rating=1)),
            score_2=Count('reviews', filter=Q(reviews__rating=2)),
            score_3=Count('reviews', filter=Q(reviews__rating=3)),
            score_4=Count('reviews', filter=Q(reviews__rating=4)),
            score_5=Count('reviews', filter=Q(reviews__rating=5)),
        )
        return qs

    def get_sold_quantity(self):
        qs = self.annotate(sold_count=Sum("packs__order_packs__quantity"))
        return qs

    def get_min_sold_pack(self):  # todo:return pack
        qs = self.annotate(min_sold_count=Min("packs__order_packs__quantity"))
        return qs

    def get_max_sold_pack(self):  # todo:return pack
        qs = self.annotate(max_sold_count=Max("packs__order_packs__quantity"))
        return qs

    def get_recommended_count(self):  # remain_test
        qs = self.annotate(recommended_count=Count(
            "reviews", filter=Q(reviews__is_recommended=True)))
        return qs

    def get_sold_price(self):
        qs = self.annotate(
            sold_price=Sum(
                F("packs__order_packs__quantity") * F("packs__order_packs__price")))
        return qs

    def filter_by_behavior(self, action=["fv", "l", "dl"], value=True, start_date=datetime(2000, 1, 1), end_date=datetime.now(), city="-",):  # remain_test
        from behavior.models import Behavior
        products_id = list(self.values_list("id", flat=True))
        if city == "-":
            return Behavior.objects.filter(
                scope=("p"),
                value=value,
                action__in=action,
                object_id__in=products_id,
                created__gte=utc.localize(start_date),
                created__lte=utc.localize(end_date)
            ).count()
        else:
            return Behavior.objects.filter(
                scope=("p"),
                value=value,
                action__in=action,
                object_id__in=products_id,
                created__gte=utc.localize(start_date),
                created__lte=utc.localize(end_date),
                user__addresses__city=city,
                user__addresses__is_default=True

            ).count()

    def get_user_bought_count(self, distinct=False):
        return self.annotate(sold=Count("packs__order_packs__order__user", distinct=distinct))

    def get_bought_users(self, distinct=False):
        if distinct:

            return self.values("packs__order_packs__order__user",).distinct()

        return self.values("packs__order_packs__order__user", )

    def get_active_packs(self, active):
        return apps.get_model('warehouse', 'Pack').objects.filter(product__in=self, is_active=active)

    # Future cart = Wishlist
    def get_count_in_wishlists(self):  # remain_test
        return self.annotate(wishlist_count=Count("packs__wishlists"))

    def get_count_in_carts(self):  # remain_test
        return self.annotate(cart_count=Count("packs__carts"))

    def get_most_frequent_in_wishlists(self):  # remain_test
        qs = self.get_count_in_wishlists()
        max_count = qs.aggregate(max=Max("wishlist_count")).get("max")
        return qs.filter(wishlist_count=max_count)

    def get_products_with_pack_color(self):
        qs = self.actives().prefetch_related(
            Prefetch(
                "packs",
                queryset=apps.get_model(
                    'warehouse', 'Pack').wise.actives().select_related('color')
            )
        )
        return qs
