from django import forms
from warehouse.models import Product ,Brand,Color

import django_filters

class ProductFilter(django_filters.FilterSet):
    brand = django_filters.ModelMultipleChoiceFilter(field_name='brand',queryset=Brand.objects.all(),widget=forms.CheckboxSelectMultiple)
    color = django_filters.ModelChoiceFilter(field_name='packs__color',queryset=Color.objects.all(),widget=forms.RadioSelect,distinct=True)
    price = django_filters.RangeFilter(field_name='packs__price')
    order = django_filters.OrderingFilter(
        # tuple-mapping retains order
        fields=(
            ('packs__price', 'price'),
            ('created','created')
        ),
    )
    
    class Meta:
        model = Product
        fields = ['title','sku']