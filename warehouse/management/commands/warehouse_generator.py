import os
import random
import secrets

from django.utils.text import slugify
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.core.management.base import (
    BaseCommand,
)

from django.core.files.uploadedfile import SimpleUploadedFile

from faker import Faker
from tqdm import tqdm

from warehouse.models import (
    Category,
    Product,
    Color,
    Brand,
    Guarantee,
    Pack,
)

User = get_user_model()
fake = Faker(['fa-IR'])


class Command(BaseCommand):
    help = _('Generate data for warehouse models')

    def add_arguments(self, parser):
        parser.add_argument('--total-parent-category', '-nPC',
                            type=int, help='Total number of parent categories')
        parser.add_argument('--total-color', '-nCol',
                            type=int, help='Total number of colors')
        parser.add_argument('--total-brand', '-nB', type=int,
                            help='Total number of brands')
        parser.add_argument('--total-guarantees', '-nG',
                            type=int, help='Total number of guarantees')

        parser.add_argument('--children-per-parent', '-CpP',
                            type=int, help='Total number of categories')
        parser.add_argument('--gchildren-per-child', '-GCpC',
                            type=int, help='Total number of sub categories')
        parser.add_argument('--product-per-category', '-PpC',
                            type=int, help='Total number of guarantees')
        parser.add_argument('--pack-per-product', '-PpP',
                            type=int, help='Total number of guarantees')

    def handle(self, *args, **kwargs):
        user = self.make_user()
        total_parent_category = kwargs.get('total_parent_category') or 10
        total_color = kwargs.get('total_color') or 5
        total_brand = kwargs.get('total_brand') or 20
        total_guarantees = kwargs.get('total_guarantees') or 5

        children_per_parent = kwargs.get('children_per_parent') or 3
        gchildren_per_child = kwargs.get('gchildren_per_child') or 5
        product_per_category = kwargs.get('product_per_category') or 50
        pack_per_product = kwargs.get('pack_per_product') or 3

        # ========> CATEGORY
        categories, children, grand_children, gchild_objs = self.create_bulk_categories(
            total_parent_category, children_per_parent, gchildren_per_child, user)
        self.stdout.write(self.style.SUCCESS(
            f'{categories} Parent Categories were created successfully.'))
        self.stdout.write(self.style.SUCCESS(
            f'{children} Categories were created successfully.'))
        self.stdout.write(self.style.SUCCESS(
            f'{grand_children} Sub Categories were created successfully.'))

        colors, total_colors = self.create_colors(total_color, user)
        self.stdout.write(self.style.SUCCESS(
            f'{total_colors} Color were created successfully.'))
        brands, total_brands = self.create_brands(total_brand, user)
        self.stdout.write(self.style.SUCCESS(
            f'{total_brands} Brand were created successfully.'))
        guarantees, total_guarantees = self.create_guarantees(
            total_guarantees, user)
        self.stdout.write(self.style.SUCCESS(
            f'{total_guarantees} Guarantee were created successfully.'))

        # ========> PRODUCT
        total_products, total_packs = self.create_product(
            gchild_objs,
            product_per_category,
            pack_per_product,
            colors,
            guarantees,
            brands,
            user
        )
        self.stdout.write(self.style.SUCCESS(
            f'{total_products} Product were created successfully.'))
        self.stdout.write(self.style.SUCCESS(
            f'{total_packs} Pack were created successfully.'))
        self.stdout.write(self.style.SUCCESS(
            f'Products in_stock field updated successfully.'))

    def make_user(self):
        user, created = User.objects.get_or_create(email='generator@gen.com')
        if created:
            user.set_password(secrets.token_urlsafe(16))
            user.is_active = False
            user.is_superuser = False
            user.save()
        return user

    def create_product(self, gchild_objs: Category, product_per_category: int, pack_per_product: int, colors: Color, guarantees: Guarantee, brands: Brand, user: User, *args, **kwargs):
        # 200 products per grand child category
        TOTAL_PRODUCTS = len(gchild_objs) * product_per_category
        # four pack per product
        TOTAL_PACKS = TOTAL_PRODUCTS * pack_per_product
        print(f'!!! GENERATING {TOTAL_PRODUCTS} PRODUCTS !!!')

        products = [
            Product(
                sku=f'sku-{i}',
                title=f'Product-{i}',
                slug=slugify(f'Product-{i}'),
                subtitle=_(f'Product-{i}'),
                summary=fake.sentence(nb_words=5),
                is_active=random.choice([True, False]),
                is_special=random.choice([True, False]),
                in_stock=0,
                # picture = SimpleUploadedFile(name=f'generated_picture.jpg', content=picture),
                picture = "product_img1.jpg",
                alternate_text = fake.sentence(nb_words=5),
                default_rating=random.randint(1, 100) / 100,
                description=fake.paragraph(nb_sentences = 8),
                category=gchild_objs[i // product_per_category],
                brand=brands[random.randint(0, len(brands)-1)],
                modified_by=user
            )
            for i in tqdm(range(TOTAL_PRODUCTS))
        ]
        products = Product.objects.bulk_create(products)

        print(f'!!! GENERATING {TOTAL_PACKS} PACKS !!!')
        packs = [
            Pack(
                sku=f'sku-{i}',
                buy_price=random.randint(100_000, 1_000_000),
                price=random.randint(1_000_000, 50_000_000),
                is_active=True if i % 2 == 0 else False,
                is_default=True if i % 4 == 0 else False,
                in_stock=random.choice(range(0, 1000)),
                is_supply=random.choice([True, False]),
                product=products[i // pack_per_product],
                guarantee=guarantees[random.randint(0, len(guarantees)-1)],
                color=colors[random.randint(0, len(colors)-1)],
                modified_by=user
            )
            for i in tqdm(range(TOTAL_PACKS))
        ]

        packs = Pack.objects.bulk_create(packs)

        print(f'!!! UPDATING PRODUCTS !!!')
        in_stocks = [
            sum(packs[i*pack_per_product +
                j].in_stock for j in range(pack_per_product))
            for i in tqdm(range(TOTAL_PRODUCTS))
        ]

        [setattr(product, 'in_stock', in_stocks[i])
            for i, product in enumerate(products)]

        Product.objects.bulk_update(products, ["in_stock"])

        return TOTAL_PRODUCTS, TOTAL_PACKS

    def create_bulk_categories(self, total_parents, child_per_parent, grandchild_per_child, user, *args, **kwargs):

        parents_obj = [Category(
            title=f'cat-{index}',
            slug=slugify(f'cat-{index}'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
            for index in range(total_parents)
        ]

        parents = Category.objects.bulk_create(parents_obj)
        Category.objects.rebuild()

        children_obj = [Category(
            title=f'child-{index}',
            slug=slugify(f'child-{index}'),
            parent=parents[index//child_per_parent],
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
            for index in range(total_parents * child_per_parent)
        ]

        children = Category.objects.bulk_create(children_obj)
        Category.objects.rebuild()

        grand_children_obj = [Category(
            title=f'gchild-{index}',
            slug=slugify(f'gchild-{index}'),
            parent=children[index//grandchild_per_child],
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
            for index in range(total_parents * child_per_parent * grandchild_per_child)
        ]

        grand_children = Category.objects.bulk_create(grand_children_obj)
        Category.objects.rebuild()

        return (
            total_parents,
            total_parents * child_per_parent,
            total_parents * child_per_parent * grandchild_per_child,
            grand_children
        )

    def create_colors(self, total: int, user: User, *args, **kwargs):
        print(f'!!! GENERATING {total} COLORS !!!')
        title_en = [f'Color-{index}' for index in range(total)]
        title_fa = [f'رنگ-{index}' for index in range(total)]
        Hex_Code = [fake.hex_color() for _ in range(total)]
        colors = [
            Color(
                title_en=title_en[j],
                title_fa=title_fa[j],
                hex_code=Hex_Code[j],
                modified_by=user
            )
            for j in range(total)
        ]

        colors = Color.objects.bulk_create(colors)

        return colors, len(title_en)

    def create_brands(self, total: int, user: User, *args, **kwargs):
        print(f'!!! GENERATING {total} BRANDS !!!')

        titles = [f'Brand-{index}' for index in range(total)]
        brands = [
            Brand(
                title=title,
                slug=slugify(title),
                modified_by=user
            )
            for title in titles
        ]
        brands = Brand.objects.bulk_create(brands)
        return brands, len(titles)

    def create_guarantees(self, total: int, user: User, *args, **kwargs):
        print(f'!!! GENERATING {total} GUARANTEES !!!')

        title_en = [f'guarantee-{index}' for index in range(total)]
        title_fa = [f'گارانتی-{index}' for index in range(total)]
        guarantees = [
            Guarantee(
                title_en=title_en[i],
                title_fa=title_fa[i],
                modified_by=user
            )
            for i in range(total)
        ]
        guarantees = Guarantee.objects.bulk_create(guarantees)
        return guarantees, len(title_en)
