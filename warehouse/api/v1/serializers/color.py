from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
)

from warehouse.models import Color


class ColorSerializer(HyperlinkedModelSerializer):
    """
    A Serializer for color instances.
    """
    url = HyperlinkedIdentityField(
        view_name='color-detail',
        lookup_field='title_en'
    )

    class Meta:
        model = Color
        fields = [
            'url',
            'hex_code',
            'title_en',
            'title_fa',
        ]
