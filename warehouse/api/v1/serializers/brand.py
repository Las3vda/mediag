from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
)

from warehouse.models import Brand


class BrandSerializer(HyperlinkedModelSerializer):
    """
    A Serializer for brand instances.
    """
    url = HyperlinkedIdentityField(
        view_name='brand-detail',
        lookup_field='slug'
    )

    class Meta:
        model = Brand
        fields = [
            'url',
            'slug',
            'title',
        ]
