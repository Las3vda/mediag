from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
)

from warehouse.models import (
    Pack
)

from warehouse.api.v1.serializers import (
    GuaranteeSerializer,
    ColorSerializer,
    ProductSerializer
)


class PackSerializer(HyperlinkedModelSerializer):
    """
    A Serializer for pack instances.
    """
    url = HyperlinkedIdentityField(
        view_name='pack-detail',
        lookup_field='sku'
    )

    guarantee = GuaranteeSerializer()
    color = ColorSerializer()
    product = ProductSerializer()

    class Meta:
        model = Pack
        fields = [
            'sku',
            'url',
            'price',
            'product',
            'color',
            'guarantee',
            'in_stock',
            'is_default',
        ]
