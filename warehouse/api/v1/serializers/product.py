from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
)

from warehouse.models import (
    Product
)

from warehouse.api.v1.serializers import (
    CategorySerializer,
    BrandSerializer
)


class ProductSerializer(HyperlinkedModelSerializer):
    """
    A Serializer for product instances.
    """
    url = HyperlinkedIdentityField(
        view_name='product-detail',
        lookup_field='slug'
    )

    # category = HyperlinkedRelatedField(
    #     view_name='category-detail',
    #     lookup_field='slug',
    #     read_only=True
    # )

    category = CategorySerializer()
    brand = BrandSerializer()

    class Meta:
        model = Product
        fields = [
            'url',
            'sku',
            'title',
            'subtitle',
            'description',
            'brand',
            'summary',
            'is_special',
            'show_default_rating',
            'default_rating',
            'picture',
            'category',
            'alternate_text',
        ]
