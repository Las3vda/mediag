from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
    HyperlinkedRelatedField
)

from warehouse.models import Category


class CategorySerializer(HyperlinkedModelSerializer):
    """
    A Serializer for category instances.
    """
    url = HyperlinkedIdentityField(
        view_name='category-detail',
        lookup_field='slug'
    )
    parent = HyperlinkedRelatedField(
        view_name='category-detail',
        lookup_field='slug',
        read_only=True
    )

    class Meta:
        model = Category
        fields = [
            'url',
            'slug',
            'title',
            'parent'
        ]
