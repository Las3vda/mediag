from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
)

from warehouse.models import Guarantee


class GuaranteeSerializer(HyperlinkedModelSerializer):
    """
    A Serializer for guarantee instances.
    """
    url = HyperlinkedIdentityField(
        view_name='guarantee-detail',
        lookup_field='title_en'
    )

    class Meta:
        model = Guarantee
        fields = [
            'url',
            'title_en',
            'title_fa',
        ]
