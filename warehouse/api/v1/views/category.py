from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter

from warehouse.api.v1.serializers import CategorySerializer

from warehouse.models import Category


class CategoryViewSet(ModelViewSet):
    """
    A viewset for viewing and editing category instances.
    """
    serializer_class = CategorySerializer
    queryset = Category.objects.filter(is_active=True).select_related('parent')

    # Filter options
    filter_backends = [
        SearchFilter
    ]
    search_fields = [
        'title'
    ]
    ordering_fields = ['title']

    # lookups
    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'
