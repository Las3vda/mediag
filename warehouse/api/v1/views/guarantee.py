from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter


from warehouse.api.v1.serializers import GuaranteeSerializer
from warehouse.models import Guarantee


class GuaranteeViewSet(ModelViewSet):
    """
    A viewset for viewing and editing Guarantee instances.
    """
    serializer_class = GuaranteeSerializer
    queryset = Guarantee.objects.all()

    # Filter options
    filter_backends = [
        SearchFilter
    ]
    search_fields = [
        'title_en',
        'title_fa',
    ]
    ordering_fields = ['title_en']

    # lookups
    lookup_field = 'title_en'
    lookup_url_kwarg = 'title_en'
