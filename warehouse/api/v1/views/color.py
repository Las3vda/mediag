from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter

from warehouse.api.v1.serializers import ColorSerializer

from warehouse.models import Color


class ColorViewSet(ModelViewSet):
    """
    A viewset for viewing and editing Color instances.
    """
    serializer_class = ColorSerializer
    queryset = Color.objects.all()

    # Filter options
    filter_backends = [
        SearchFilter
    ]
    search_fields = [
        'title_en',
        'title_fa',
        'hex_code'
    ]
    ordering_fields = ['title_en']

    # lookups
    lookup_field = 'title_en'
    lookup_url_kwarg = 'title_en'
