from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)
from django_filters.rest_framework import DjangoFilterBackend

from warehouse.api.v1.serializers import PackSerializer

from warehouse.models import Pack


class PackViewSet(ModelViewSet):
    """
    A viewset for viewing and editing pack instances.
    """

    serializer_class = PackSerializer
    queryset = Pack.wise.actives().select_related(
        'color', 'guarantee', 'product__category', 'product__brand', 'product__category__parent')

    # Filter options
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    filterset_fields = ['is_default', 'price']
    search_fields = ['sku']
    ordering_fields = ['sku']

    # lookups
    lookup_field = 'sku'
    lookup_url_kwarg = 'sku'
