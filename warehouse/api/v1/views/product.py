from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)
from django_filters.rest_framework import DjangoFilterBackend

from warehouse.api.v1.serializers import ProductSerializer

from warehouse.models import Product


class ProductViewSet(ModelViewSet):
    """
    A viewset for viewing and editing product instances.
    """

    serializer_class = ProductSerializer
    queryset = Product.wise.actives().get_related_category(
    ).select_related('category__parent', 'brand')

    # Filter options
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    filterset_fields = ['is_special']
    search_fields = ['sku', 'title', 'subtitle']
    ordering_fields = ['default_rating']

    # lookups
    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'
