from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter

from warehouse.api.v1.serializers import BrandSerializer

from warehouse.models import Brand


class BrandViewSet(ModelViewSet):
    """
    A viewset for viewing and editing brand instances.
    """
    serializer_class = BrandSerializer
    queryset = Brand.objects.all()

    # Filter options
    filter_backends = [
        SearchFilter
    ]
    search_fields = [
        'title'
    ]
    ordering_fields = ['title']

    # lookups
    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'
