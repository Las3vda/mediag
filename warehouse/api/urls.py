from django.urls import path, include

from rest_framework import routers

from warehouse.api.v1.views import (
    CategoryViewSet,
    ProductViewSet,
    BrandViewSet,
    ColorViewSet,
    GuaranteeViewSet,
    PackViewSet
)

router = routers.SimpleRouter()
router.register(r'products', ProductViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'brands', BrandViewSet)
router.register(r'colors', ColorViewSet)
router.register(r'guarantees', GuaranteeViewSet)
router.register(r'packs', PackViewSet)

urlpatterns = [
    path('v1/', include(router.urls)),
]
