import secrets

from django.utils.text import slugify
from django.contrib.auth import get_user_model

from logistic.models import Logistic
from user.models import (
    Address,
    Profile)
from cart.models import (
    Order,
    PackOrder
)
from warehouse.models import(
    Category,
    Color,
    Guarantee,
    Pack,
    Brand,
    Product
)


class GetSoldCountFakeData():
    def __init__(self) -> None:
        self.create_data()

    def create_data():
        User = get_user_model()

        user = User(
            email='user-0@gmail.com',
            password=secrets.token_urlsafe(16),
            phone_number='091333333310',
            first_name='first-0',
            last_name='last-0',
        )

        user1 = User(
            email='user-1@gmail.com',
            password=secrets.token_urlsafe(16),
            phone_number='091333333311',
            first_name='first-1',
            last_name='last-1',
        )

        user2 = User(
            email='user-2@gmail.com',
            password=secrets.token_urlsafe(16),
            phone_number='091333333312',
            first_name='first-2',
            last_name='last-2',
        )
        User.objects.bulk_create([user, user1, user2])
        profile = Profile(
            nickname='nickname',
            gender=('p'),
            job='job',
            national_code='national_code',
            user=user,
            is_completed=True
        )
        profile1 = Profile(
            nickname='nickname-1',
            gender=('m'),
            job='job-1',
            national_code='national_code-1',
            user=user1,
            is_completed=True
        )
        profile2 = Profile(
            nickname='nickname-2',
            gender=('f'),
            job='job-2',
            national_code='national_code-2',
            user=user2,
            is_completed=False
        )
        Profile.objects.bulk_create([profile, profile1, profile2])
        address = Address(
            title='Address-0',
            country="country",
            state="state-0",
            city="city-0",
            postal_address="postal address",
            postal_code="postal code",
            description='description-0',
            is_default=True,
            user=user1
        )
        address1 = Address(
            title='Address-1',
            country="country",
            state="state",
            city="city",
            postal_address="postal address",
            postal_code="postal code",
            description='description-1',
            is_default=True,
            user=user1
        )
        address2 = Address(
            title='Address-2',
            country="country",
            state="state-2",
            city="city-2",
            postal_address="postal address",
            postal_code="postal code",
            description='description-2',
            is_default=False,
            user=user2
        )

        address3 = Address(
            title='Address-3',
            country="country",
            state="state-1",
            city="city-1",
            postal_address="postal address",
            postal_code="postal code",
            description='description-3',
            is_default=False,
            user=user2
        )

        address4 = Address(
            title='Address-4',
            country="country",
            state="state-4",
            city="city-4",
            postal_address="postal address",
            postal_code="postal code",
            description='description-4',
            is_default=False,
            user=user2
        )

        Address.objects.bulk_create(
            [address, address1, address2, address3, address4])
        category1 = Category(
            title=f'cat-1',
            slug=slugify(f'cat-1'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
        category2 = Category(
            title=f'cat-2',
            slug=slugify(f'cat-2'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
        category3 = Category(
            title=f'cat-3',
            slug=slugify(f'cat-3'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
        Category.objects.bulk_create([category1, category2, category3])
        color = Color.objects.create(
            title_en="red",
            title_fa="قرمز",
            hex_code="#030403",
            modified_by=user
        )
        guarantee = Guarantee.objects.create(
            title_en="sam service",
            title_fa="سام سرویس",
            modified_by=user
        )
        brand1 = Brand(
            title="brand1",
            slug=slugify("brand1"),
            modified_by=user
        )
        brand2 = Brand(
            title="brand2",
            slug=slugify("brand2"),
            modified_by=user
        )
        brand3 = Brand(
            title="brand3",
            slug=slugify("brand3"),
            modified_by=user
        )
        Brand.objects.bulk_create([brand1, brand2, brand3])
        product1 = Product(
            sku="sku-1",
            title=f'Product-1',
            slug=slugify('Product-1'),
            subtitle='Product-1',
            summary="summary",
            is_active=False,
            is_special=False,
            in_stock=0,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category1,
            brand=brand1,
            modified_by=user,
            show_default_rating=False
        )
        product2 = Product(
            sku="sku-2",
            title=f'Product-2',
            slug=slugify('Product-2'),
            subtitle='Product-2',
            summary="summary",
            is_active=False,
            is_special=False,
            in_stock=90,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category2,
            brand=brand2,
            modified_by=user,
            show_default_rating=True
        )
        product3 = Product(
            sku="sku-3",
            title=f'Product-3',
            slug=slugify('Product-3'),
            subtitle='Product-3',
            summary="summary",
            is_active=True,
            is_special=False,
            in_stock=210,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category3,
            brand=brand3,
            modified_by=user,
            show_default_rating=True

        )
        product4 = Product(
            sku="sku-4",
            title=f'Product-4',
            slug=slugify('Product-4'),
            subtitle='Product-4',
            summary="summary",
            is_active=True,
            is_special=False,
            in_stock=1000,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category1,
            brand=brand3,
            modified_by=user,
            show_default_rating=True
        )
        Product.objects.bulk_create([product1, product2, product3, product4])
        pack1 = Pack(
            sku='sku-1',
            buy_price=70000,
            price=100000,
            is_active=True,
            is_default=True,
            in_stock=120,
            is_supply=True,
            product=product1,
            guarantee=guarantee,
            color=color,
            modified_by=user
        )
        pack2 = Pack(
            sku='sku-2',
            buy_price=7450,
            price=10000,
            is_active=True,
            is_default=True,
            in_stock=120,
            is_supply=True,
            product=product2,
            guarantee=guarantee,
            color=color,
            modified_by=user
        )
        pack3 = Pack(
            sku='sku-3',
            buy_price=3450,
            price=4000,
            is_active=True,
            is_default=True,
            in_stock=120,
            is_supply=True,
            product=product2,
            guarantee=guarantee,
            color=color,
            modified_by=user
        )
        pack4 = Pack(
            sku='sku-4',
            buy_price=4450,
            price=5000,
            is_active=True,
            is_default=True,
            in_stock=120,
            is_supply=True,
            product=product3,
            guarantee=guarantee,
            color=color,
            modified_by=user
        )
        pack5 = Pack(
            sku='sku-5',
            buy_price=4450,
            price=5000,
            is_active=True,
            is_default=True,
            in_stock=120,
            is_supply=True,
            product=product4,
            guarantee=guarantee,
            color=color,
            modified_by=user
        )
        Pack.objects.bulk_create([pack1, pack2, pack3, pack4, pack5])
        logistic = Logistic.objects.create(
            title="fakelogistic",
            price=50_000,
            is_active=True,
            modified_by=user
        )
        order1 = Order(
            user=user,
            sku="SWOF-1",
            logistic=logistic,
            status='P',
            footnote='footnote 1',
            discount_code='discount-code 1',
            receiver_name='receiver-name 1',
            receiver_phone='receiver-phone 1',
            modified_by=user,
            city="city-1",
            state="state-3"
        )
        order2 = Order(
            user=user1,
            sku="SWOF-2",
            logistic=logistic,
            status='S',
            footnote='footnote 2',
            discount_code='discount-code 2',
            receiver_name='receiver-name 2',
            receiver_phone='receiver-phone 2',
            modified_by=user,
            city="city-1",
            state="state-1"
        )
        order3 = Order(
            user=user2,
            sku="SWOF-3",
            logistic=logistic,
            status='W',
            footnote='footnote 3',
            discount_code='discount-code 3',
            receiver_name='receiver-name 3',
            receiver_phone='receiver-phone 3',
            modified_by=user,
            city="city-0",
            state="state-0"
        )
        order4 = Order(
            user=user,
            sku="SWOF-4",
            logistic=logistic,
            status='D',
            footnote='footnote 4',
            discount_code='discount-code 4',
            receiver_name='receiver-name 4',
            receiver_phone='receiver-phone 4',
            modified_by=user,
            city="city-2",
            state="state-0"
        )
        order5 = Order(
            user=user1,
            sku="SWOF-5",
            logistic=logistic,
            status='P',
            footnote='footnote 5',
            discount_code='discount-code 5',
            receiver_name='receiver-name 5',
            receiver_phone='receiver-phone 5',
            modified_by=user,
            city="city-0",
            state="state-0"
        )
        Order.objects.bulk_create([order1, order2, order3, order4, order5])
        packorder = PackOrder(
            order=order1,
            pack=pack1,
            quantity=20,
            title="title",
            guarantee="guarantee-1",
            color="color",
            price=100000,
            buy_price=70000
        )
        packorder2 = PackOrder(
            order=order2,
            pack=pack2,
            quantity=30,
            title="title",
            guarantee="guarantee-3",
            color="color-1",
            price=30000,
            buy_price=25480
        )
        packorder3 = PackOrder(
            order=order3,
            pack=pack3,
            quantity=10,
            title="title",
            guarantee="guarantee-4",
            color="color",
            price=50000,
            buy_price=45480
        )
        packorder4 = PackOrder(
            order=order4,
            pack=pack3,
            quantity=30,
            title="title",
            guarantee="guarantee-3",
            color="color-1",
            price=40000,
            buy_price=30000
        )
        packorder5 = PackOrder(
            order=order5,
            pack=pack2,
            quantity=10,
            title="title",
            guarantee="guarantee-4",
            color="color-3",
            price=90000,
            buy_price=85480
        )
        packorder6 = PackOrder(
            order=order5,
            pack=pack4,
            quantity=10,
            title="title",
            guarantee="guarantee-4",
            color="color-3",
            price=90000,
            buy_price=85480
        )
        packorder7 = PackOrder(
            order=order5,
            pack=pack5,
            quantity=40,
            title="title",
            guarantee="guarantee-4",
            color="color-3",
            price=880000,
            buy_price=55480
        )
        packorder8 = PackOrder(
            order=order4,
            pack=pack4,
            quantity=40,
            title="title",
            guarantee="guarantee-4",
            color="color-3",
            price=880000,
            buy_price=55480
        )
        PackOrder.objects.bulk_create(
            [packorder, packorder2, packorder3, packorder4, packorder5, packorder6, packorder7, packorder8])
