import secrets

from django.utils.text import slugify
from django.contrib.auth import get_user_model

from user.models import (
    Address,
    Profile)
from warehouse.models import(
    Category,
    Brand,
    Product
)


class InStockFakeData():
    def __init__(self) -> None:
        self.create_data()

    def create_data():
        User = get_user_model()

        user = User(
            email='user-0@gmail.com',
            password=secrets.token_urlsafe(16),
            phone_number='091333333310',
            first_name='first-0',
            last_name='last-0',
        )

        user1 = User(
            email='user-1@gmail.com',
            password=secrets.token_urlsafe(16),
            phone_number='091333333311',
            first_name='first-1',
            last_name='last-1',
        )

        user2 = User(
            email='user-2@gmail.com',
            password=secrets.token_urlsafe(16),
            phone_number='091333333312',
            first_name='first-2',
            last_name='last-2',
        )
        User.objects.bulk_create([user, user1, user2])
        profile = Profile(
            nickname='nickname',
            gender=('p'),
            job='job',
            national_code='national_code',
            user=user,
            is_completed=True
        )
        profile1 = Profile(
            nickname='nickname-1',
            gender=('m'),
            job='job-1',
            national_code='national_code-1',
            user=user1,
            is_completed=True
        )
        profile2 = Profile(
            nickname='nickname-2',
            gender=('f'),
            job='job-2',
            national_code='national_code-2',
            user=user2,
            is_completed=False
        )
        Profile.objects.bulk_create([profile, profile1, profile2])
        address = Address(
            title='Address-0',
            country="country",
            state="state-0",
            city="city-0",
            postal_address="postal address",
            postal_code="postal code",
            description='description-0',
            is_default=True,
            user=user1
        )
        address1 = Address(
            title='Address-1',
            country="country",
            state="state",
            city="city",
            postal_address="postal address",
            postal_code="postal code",
            description='description-1',
            is_default=True,
            user=user1
        )
        address2 = Address(
            title='Address-2',
            country="country",
            state="state-2",
            city="city-2",
            postal_address="postal address",
            postal_code="postal code",
            description='description-2',
            is_default=False,
            user=user2
        )

        address3 = Address(
            title='Address-3',
            country="country",
            state="state-1",
            city="city-1",
            postal_address="postal address",
            postal_code="postal code",
            description='description-3',
            is_default=False,
            user=user2
        )

        address4 = Address(
            title='Address-4',
            country="country",
            state="state-4",
            city="city-4",
            postal_address="postal address",
            postal_code="postal code",
            description='description-4',
            is_default=False,
            user=user2
        )

        Address.objects.bulk_create(
            [address, address1, address2, address3, address4])
        category1 = Category(
            title=f'cat-1',
            slug=slugify(f'cat-1'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
        category2 = Category(
            title=f'cat-2',
            slug=slugify(f'cat-2'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
        category3 = Category(
            title=f'cat-3',
            slug=slugify(f'cat-3'),
            parent=None,
            modified_by=user,
            lft=0,
            rght=0,
            level=0,
            tree_id=0
        )
        Category.objects.bulk_create([category1, category2, category3])
        brand1 = Brand(
            title="brand1",
            slug=slugify("brand1"),
            modified_by=user
        )
        brand2 = Brand(
            title="brand2",
            slug=slugify("brand2"),
            modified_by=user
        )
        brand3 = Brand(
            title="brand3",
            slug=slugify("brand3"),
            modified_by=user
        )
        Brand.objects.bulk_create([brand1, brand2, brand3])
        product1 = Product(
            sku="sku-1",
            title=f'Product-1',
            slug=slugify('Product-1'),
            subtitle='Product-1',
            summary="summary",
            is_active=False,
            is_special=False,
            in_stock=0,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category1,
            brand=brand1,
            modified_by=user
        )
        product2 = Product(
            sku="sku-2",
            title=f'Product-2',
            slug=slugify('Product-2'),
            subtitle='Product-2',
            summary="summary",
            is_active=False,
            is_special=False,
            in_stock=90,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category2,
            brand=brand2,
            modified_by=user
        )
        product3 = Product(
            sku="sku-3",
            title=f'Product-3',
            slug=slugify('Product-3'),
            subtitle='Product-3',
            summary="summary",
            is_active=True,
            is_special=False,
            in_stock=210,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category3,
            brand=brand3,
            modified_by=user
        )
        product4 = Product(
            sku="sku-4",
            title=f'Product-4',
            slug=slugify('Product-4'),
            subtitle='Product-4',
            summary="summary",
            is_active=True,
            is_special=False,
            in_stock=1000,
            picture="product_img1.jpg",
            alternate_text="alternate text",
            default_rating=3,
            description="discription",
            category=category1,
            brand=brand3,
            modified_by=user
        )
        Product.objects.bulk_create([product1, product2, product3, product4])
