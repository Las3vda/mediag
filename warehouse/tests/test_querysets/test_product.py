from datetime import datetime

from django.test import TestCase
from django.contrib.auth import get_user_model

from painless.utils.decorators.time_decorators import not_more_than
from warehouse.models import (
    Product,
    Brand,
    Category
)
from warehouse.tests.test_data.product import (
    RangeStockFakeData,
    ActivesFakeData,
    DeactivesFakeData,
    FilterByPriceRangeFakeData,
    FilterBySkuFakeData,
    FilterOrdersByDateFakeData,
    GetBoughtUsersFakeData,
    GetUserBoughtCountFakeData,
    GetMaxSoldPackFakeData,
    GetMinSoldPackFakeData,
    GetRelatedBrandFakeData,
    GetRelatedCategoryFakeData,
    GetSoldPriceFakeData,
    GetSoldQuantityFakeData,
    HasDefaultRatingFakeData,
    OutOfStockFakeData,
    InStockFakeData,
    SpecialsFakeData,
)

User = get_user_model()


class ProductQuerySetTestCase(TestCase):

    @not_more_than(0.09)
    def test_range_stock_product_queryset(self):
        RangeStockFakeData.create_data()
        products = list(Product.wise.all())
        self.assertQuerysetEqual(
            Product.wise.range_stock(level="empty"),
            [products[0]],
            ordered=False
        )
        # self.assertListEqual(
        #     list(
        #         Product.wise.range_stock(level="empty")
        #     ),
        #     [
        #         products[0]
        #     ],
        #     "level=>empty"
        # )
        self.assertQuerysetEqual(
            Product.wise.range_stock(level="low"),
            [products[1]],
            ordered=False
        )
        # self.assertListEqual(
        #     list(
        #         Product.wise.range_stock(level="low")
        #     ),
        #     [
        #         products[1]
        #     ],
        #     "level=>low"
        # )
        self.assertQuerysetEqual(
            Product.wise.range_stock(level="medium"),
            [products[2]],
            ordered=False
        )
        # self.assertListEqual(
        #     list(
        #         Product.wise.range_stock(level="medium")
        #     ),
        #     [
        #         products[2]
        #     ],
        #     "level=>medium"
        # )
        self.assertQuerysetEqual(
            Product.wise.range_stock(level="huge"),
            [products[3]],
            ordered=False
        )
        # self.assertListEqual(
        #     list(
        #         Product.wise.range_stock(level="huge")
        #     ),
        #     [
        #         products[3]
        #     ],
        #     "level=>huge"
        # )

    @not_more_than(0.09)
    def test_actives_product_queryset(self):
        ActivesFakeData.create_data()
        product1 = Product.wise.get(sku="sku-3")
        product2 = Product.wise.get(sku="sku-4")

        self.assertQuerysetEqual(
            Product.wise.actives(),
            [
                product2,
                product1
            ],
            ordered=False
        )
        # self.assertListEqual(
        #     list(
        #         Product.wise.actives()
        #     ),
        #     [
        #         product1,
        #         product2

        #     ]
        # )

    @not_more_than(0.09)
    def test_deactives_product_queryset(self):
        DeactivesFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        product2 = Product.wise.get(sku="sku-2")
        self.assertQuerysetEqual(
            Product.wise.deactives(),
            [
                product2,
                product1
            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_in_stock_product_queryset(self):
        InStockFakeData.create_data()
        product1 = Product.wise.get(sku="sku-2")
        product2 = Product.wise.get(sku="sku-3")
        product3 = Product.wise.get(sku="sku-4")
        self.assertQuerysetEqual(
            Product.wise.in_stock(),
            [
                product3,
                product2,
                product1
            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_out_of_stock_product_queryset(self):
        OutOfStockFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        self.assertQuerysetEqual(
            Product.wise.out_of_stock(),
            [
                product1,
            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_specials_product_queryset(self):
        SpecialsFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        product2 = Product.wise.get(sku="sku-3")
        self.assertQuerysetEqual(
            Product.wise.specials(),
            [
                product1,
                product2,
            ],

            ordered=False
        )

    @not_more_than(0.09)
    def test_has_default_rating_product_queryset(self):
        HasDefaultRatingFakeData.create_data()
        product1 = Product.wise.get(sku="sku-2")
        product2 = Product.wise.get(sku="sku-3")
        product3 = Product.wise.get(sku="sku-4")
        self.assertQuerysetEqual(
            Product.wise.has_default_rating(),
            [
                product1,
                product2,
                product3
            ],

            ordered=False
        )

    @not_more_than(0.09)
    def test_filter_by_sku_product_queryset(self):
        FilterBySkuFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        product2 = Product.wise.get(sku="sku-2")
        product3 = Product.wise.get(sku="sku-3")
        product4 = Product.wise.get(sku="sku-4")
        self.assertQuerysetEqual(
            Product.wise.filter_by_sku(sku="sku-1"),
            [
                product1,

            ],

            ordered=False
        )

        self.assertQuerysetEqual(
            Product.wise.filter_by_sku(sku="sku-2"),
            [
                product2,

            ],

            ordered=False
        )

        self.assertQuerysetEqual(
            Product.wise.filter_by_sku(sku="sku-3"),
            [
                product3,

            ],

            ordered=False
        )

        self.assertQuerysetEqual(
            Product.wise.filter_by_sku(sku="sku-4"),
            [
                product4,

            ],

            ordered=False
        )

    @not_more_than(0.09)
    def test_filter_by_price_range_product_queryset(self):
        FilterByPriceRangeFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        product2 = Product.wise.get(sku="sku-2")
        product3 = Product.wise.get(sku="sku-3")
        product4 = Product.wise.get(sku="sku-4")
        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(1, 600),
            [
                product2,
            ],

            ordered=False
        )
        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(1, 40000),
            [
                product2, product4,
            ],

            ordered=False
        )

        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(30000, 120000),
            [
                product1, product3, product4,
            ],

            ordered=False
        )

        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(1, 1000000000),
            [
                product3,
                product1,
                product4,
                product2
            ],

            ordered=False
        )
        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(3845600, 3994000),
            list(),
            ordered=False
        )
        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(200000000, 3000000000),
            list(),
            ordered=False
        )
        self.assertQuerysetEqual(
            Product.wise.filter_by_price_range(120000, 400000),
            [product3],
            ordered=False
        )

    @not_more_than(0.09)
    def test_get_sold_price_product_queryset(self):
        GetSoldPriceFakeData.create_data()

        self.assertListEqual(
            [
                Product.wise.get_sold_price().get(sku="sku-1").sold_price,
                Product.wise.get_sold_price().get(sku="sku-2").sold_price,
                Product.wise.get_sold_price().get(sku="sku-3").sold_price,
                Product.wise.get_sold_price().get(sku="sku-4").sold_price,
            ],
            [
                2000000,
                3500000,
                36100000,
                35200000
            ]
        )

    @not_more_than(0.09)
    def test_get_sold_quantity_product_queryset(self):
        GetSoldQuantityFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        product2 = Product.wise.get(sku="sku-2")
        product3 = Product.wise.get(sku="sku-3")
        product4 = Product.wise.get(sku="sku-4")
        self.assertListEqual(
            [
                Product.wise.get_sold_quantity().get(sku="sku-1").sold_count,
                Product.wise.get_sold_quantity().get(sku="sku-2").sold_count,
                Product.wise.get_sold_quantity().get(sku="sku-3").sold_count,
                Product.wise.get_sold_quantity().get(sku="sku-4").sold_count,
            ],
            [
                20,
                80,
                50,
                40
            ]
        )

    @not_more_than(0.09)
    def test_get_user_bought_count_product_queryset(self):
        GetUserBoughtCountFakeData.create_data()
        self.assertListEqual(
            [
                Product.wise.get_user_bought_count().get(sku="sku-1").sold,
                Product.wise.get_user_bought_count(
                    distinct=True).get(sku="sku-1").sold,
                Product.wise.get_user_bought_count().get(sku="sku-2").sold,
                Product.wise.get_user_bought_count(
                    distinct=True).get(sku="sku-2").sold,
                Product.wise.get_user_bought_count().get(sku="sku-3").sold,
                Product.wise.get_user_bought_count(
                    distinct=True).get(sku="sku-3").sold,
                Product.wise.get_user_bought_count().get(sku="sku-4").sold,
                Product.wise.get_user_bought_count(
                    distinct=True).get(sku="sku-4").sold,
            ],

            [
                1, 1,
                4, 3,
                2, 2,
                1, 1
            ],

        )

    @not_more_than(0.09)
    def test_get_related_brand_product_queryset(self):
        GetRelatedBrandFakeData.create_data()
        brand1 = Brand.wise.get(title="brand1")
        brand2 = Brand.wise.get(title="brand2")
        brand3 = Brand.wise.get(title="brand3")
        self.assertQuerysetEqual(
            [
                Product.wise.get_related_brand()[0].brand,
                Product.wise.get_related_brand()[1].brand,
                Product.wise.get_related_brand()[2].brand,
                Product.wise.get_related_brand()[3].brand,
            ],

            [
                brand1,
                brand3,
                brand2,
                brand3,
            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_get_related_category_product_queryset(self):
        GetRelatedCategoryFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        category3 = Category.wise.get(title="cat-3")
        self.assertQuerysetEqual(
            [
                Product.wise.get_related_category()[0].category,
                Product.wise.get_related_category()[1].category,
                Product.wise.get_related_category()[2].category,
                Product.wise.get_related_category()[3].category,
            ],

            [
                category1,
                category2,
                category3,
                category1,
            ],
            ordered=False
        )

    # @not_more_than(0.09)
    # def test_get_related_orders_product_queryset(self):
    #     GetRelatedOrdersFakeData.create_data()
    #     product1 = Product.wise.get(sku="sku-1")
    #     product2 = Product.wise.get(sku="sku-2")
    #     product3 = Product.wise.get(sku="sku-3")
    #     product4 = Product.wise.get(sku="sku-4")
    #     self.assertQuerysetEqual(
    #         [
    #             Product.wise.get_related_orders().all(),

    #         ],

    #         [

    #         ],
    #     )

    # @not_more_than(0.09)  # test err
    # def test_get_bought_users_product_queryset(self):
    #     GetBoughtUsersFakeData.create_data()
    #     self.assertQuerysetEqual(

    #         Product.wise.get_bought_users(distinct=True).values_list(
    #             "packs__order_packs__order__user__email"),

    #         [
    #             ("user-0@gmail.com",),
    #             ("user-1@gmail.com",),
    #             ("user-2@gmail.com",),
    #         ],

    #         ordered=False
    #     )
        # self.assertQuerysetEqual(
        #     list(Product.wise.get_bought_users().values_list(
        #         "packs__order_packs__order__user__email")),

        #     [
        #         ("user-0@gmail.com",),
        #         ("user-1@gmail.com",),
        #         ("user-1@gmail.com",),
        #         ("user-0@gmail.com",),
        #         ("user-2@gmail.com",),
        #         ("user-0@gmail.com",),
        #         ("user-1@gmail.com",),
        #         ("user-1@gmail.com",)

        #     ],

        #     ordered=False
        # )

    @not_more_than(0.09)
    def test_get_max_sold_pack_product_queryset(self):
        GetMaxSoldPackFakeData.create_data()
        self.assertQuerysetEqual(
            list(
                Product.wise.get_max_sold_pack().values_list(
                    "max_sold_count"),
            ),

            [
                (20,),
                (40,),
                (30,),
                (40,),

            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_get_min_sold_pack_product_queryset(self):
        GetMinSoldPackFakeData.create_data()
        self.assertQuerysetEqual(
            list(
                Product.wise.get_min_sold_pack().values_list(
                    "min_sold_count"),
            ),

            [
                (10,),
                (10,),
                (20,),
                (40,),
            ],
            ordered=False

        )

    @not_more_than(0.09)
    def test_filter_orders_by_date_product_queryset(self):
        FilterOrdersByDateFakeData.create_data()
        product1 = Product.wise.get(sku="sku-1")
        product2 = Product.wise.get(sku="sku-2")
        product3 = Product.wise.get(sku="sku-3")
        product4 = Product.wise.get(sku="sku-4")
        self.assertQuerysetEqual(
            list(
                Product.wise.filter_orders_by_date(
                    end_date=datetime(2030, 1, 1))
            ),

            [
                product1,
                product2,
                product2,
                product2,
                product3,
                product2,
                product3,
                product4
            ],
            ordered=False,


        )
