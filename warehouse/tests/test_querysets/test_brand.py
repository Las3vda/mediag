from datetime import datetime

from django.test import TestCase
from django.contrib.auth import get_user_model

from painless.utils.decorators.time_decorators import not_more_than
from warehouse.models import (
    Brand
)
from warehouse.tests.test_data.brand import (
    GetSoldCountFakeData,
    GetUserBoughtFakeData,
    GetUserBoughtCountFakeData,
    FilterByTitleFakeData,
    FilterOrdersByCityFakeData,
    FilterOrdersByDateFakeData,
    FilterOrdersByStatusFakeData,
)

User = get_user_model()


class BrandQuerySetTestCase(TestCase):

    @not_more_than(0.09)
    def test_get_user_bought_count_brand_queryset(self):
        GetUserBoughtCountFakeData.create_data()
        self.assertListEqual(
            [
                Brand.wise.get_user_bought_count().get(title="brand1").bought,
                Brand.wise.get_user_bought_count(
                    distinct=True).get(title="brand1").bought,
                Brand.wise.get_user_bought_count().get(title="brand2").bought,
                Brand.wise.get_user_bought_count(
                    distinct=True).get(title="brand2").bought,
                Brand.wise.get_user_bought_count().get(title="brand3").bought,
                Brand.wise.get_user_bought_count(
                    distinct=True).get(title="brand3").bought,
            ],
            [
                1, 1,
                4, 3,
                3, 2
            ]

        )

    @not_more_than(0.09)
    def test_get_sold_quantity_brand_queryset(self):
        GetSoldCountFakeData.create_data()
        self.assertQuerysetEqual(
            [
                Brand.wise.get_sold_count().get(title="brand1").sold,
                Brand.wise.get_sold_count().get(title="brand2").sold,
                Brand.wise.get_sold_count().get(title="brand3").sold,
            ],
            [
                20,
                80,
                90
            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_get_bought_users_brand_queryset(self):
        GetUserBoughtFakeData.create_data()

        self.assertQuerysetEqual(

            Brand.wise.get_bought_users().values_list(
                "products__packs__order_packs__order__user__email"),



            [
                ("user-0@gmail.com",),
                ("user-0@gmail.com",),
                ("user-0@gmail.com",),
                ("user-1@gmail.com",),
                ("user-1@gmail.com",),
                ("user-1@gmail.com",),
                ("user-1@gmail.com",),
                ("user-2@gmail.com",),

            ],


            ordered=False
        )

        self.assertQuerysetEqual(

            Brand.wise.get_bought_users(distinct=True).values_list(
                "products__packs__order_packs__order__user__email"),

            [
                ("user-0@gmail.com",),
                ("user-1@gmail.com",),
                ("user-2@gmail.com",),
            ],

            ordered=False
        )

    @ not_more_than(0.09)
    def test_filter_by_title_brand_queryset(self):
        FilterByTitleFakeData.create_data()
        brand1 = Brand.wise.get(title="brand1")
        brand2 = Brand.wise.get(title="brand2")
        brand3 = Brand.wise.get(title="brand3")
        self.assertListEqual(
            [
                list(Brand.wise.filter_by_title(title="brand1")),
                list(Brand.wise.filter_by_title(title="brand2")),
                list(Brand.wise.filter_by_title(title="brand3")),
            ],
            [
                [brand1],
                [brand2],
                [brand3],
            ]
        )

    @ not_more_than(0.09)
    def test_filter_orders_by_date_brand_queryset(self):
        FilterOrdersByDateFakeData.create_data()
        brand1 = Brand.wise.get(title="brand1")
        brand2 = Brand.wise.get(title="brand2")
        brand3 = Brand.wise.get(title="brand3")
        self.assertQuerysetEqual(
            list(
                Brand.wise.filter_order_by_date(
                    end_date=datetime(2030, 1, 1))
            ),

            [
                brand1,
                brand2,
                brand2,
                brand2,
                brand3,
                brand2,
                brand3,
                brand3

            ],
            ordered=False

        )

    @ not_more_than(0.09)
    def test_filter_order_by_city_brand_queryset(self):
        FilterOrdersByCityFakeData.create_data()
        brand1 = Brand.wise.get(title="brand1")
        brand2 = Brand.wise.get(title="brand2")
        brand3 = Brand.wise.get(title="brand3")

        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_city("city-1"),


            [
                brand1,
                brand2
            ],


            ordered=False
        )
        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_city("city-0"),

            [
                brand2,
                brand2,
                brand3,
                brand3
            ],
            ordered=False
        )

    @ not_more_than(0.09)
    def test_filter_orders_by_status_brand_queryset(self):
        FilterOrdersByStatusFakeData.create_data()
        brand1 = Brand.wise.get(title="brand1")
        brand2 = Brand.wise.get(title="brand2")
        brand3 = Brand.wise.get(title="brand3")

        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_statuses("W"),

            [brand2],
            ordered=False
        )
        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_statuses("X"),

            list(),
            ordered=False
        )
        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_statuses("E"),

            list(),
            ordered=False
        )
        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_statuses("P"),

            [brand1, brand2, brand3, brand3],
            ordered=False
        )
        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_statuses("WSDE"),

            [brand2, brand2, brand2, brand3],
            ordered=False
        )
        self.assertQuerysetEqual(

            Brand.wise.filter_order_by_statuses("WSD"),

            [brand2, brand2, brand2, brand3],
            ordered=False
        )
