from datetime import datetime

from django.test import TestCase
from django.contrib.auth import get_user_model

from painless.utils.decorators.time_decorators import not_more_than
from warehouse.models import (
    Category
)
from warehouse.tests.test_data.category import (
    GetSoldCountFakeData,
    GetUserBoughtFakeData,
    GetUserBoughtCountFakeData,
    FilterByTitleFakeData,
    FilterOrdersByCityFakeData,
    FilterOrdersByDateFakeData,
    FilterOrdersByStatusFakeData,
    ActivesFakeData,
    DeactivesFakeData
)

User = get_user_model()


class CategoryQuerySetTestCase(TestCase):

    @not_more_than(0.09)
    def test_get_user_bought_count_category_queryset(self):
        GetUserBoughtCountFakeData.create_data()
        self.assertListEqual(
            [
                Category.wise.get_user_bought_count().get(title="cat-1").bought,
                Category.wise.get_user_bought_count(
                    distinct=True).get(title="cat-1").bought,
                Category.wise.get_user_bought_count().get(title="cat-2").bought,
                Category.wise.get_user_bought_count(
                    distinct=True).get(title="cat-2").bought,
                Category.wise.get_user_bought_count().get(title="cat-3").bought,
                Category.wise.get_user_bought_count(
                    distinct=True).get(title="cat-3").bought,
            ],
            [
                2, 2,
                4, 3,
                2, 2
            ]

        )

    @not_more_than(0.09)
    def test_get_sold_quantity_category_queryset(self):
        GetSoldCountFakeData.create_data()
        self.assertQuerysetEqual(
            [
                Category.wise.get_sold_count().get(title="cat-1").sold,
                Category.wise.get_sold_count().get(title="cat-2").sold,
                Category.wise.get_sold_count().get(title="cat-3").sold,
            ],
            [
                50,
                60,
                80,
            ],
            ordered=False
        )

    @not_more_than(0.09)
    def test_get_bought_users_category_queryset(self):
        GetUserBoughtFakeData.create_data()

        self.assertQuerysetEqual(

            Category.wise.get_bought_users().values_list(
                "products__packs__order_packs__order__user__email"),



            [
                ("user-0@gmail.com",),
                ("user-0@gmail.com",),
                ("user-0@gmail.com",),
                ("user-1@gmail.com",),
                ("user-1@gmail.com",),
                ("user-1@gmail.com",),
                ("user-1@gmail.com",),
                ("user-2@gmail.com",),

            ],


            ordered=False
        )

        self.assertQuerysetEqual(

            Category.wise.get_bought_users(distinct=True).values_list(
                "products__packs__order_packs__order__user__email"),

            [
                ("user-0@gmail.com",),
                ("user-1@gmail.com",),
                ("user-2@gmail.com",),
            ],

            ordered=False
        )

    @ not_more_than(0.09)
    def test_filter_by_title_category_queryset(self):
        FilterByTitleFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        category3 = Category.wise.get(title="cat-3")
        self.assertListEqual(
            [
                list(Category.wise.filter_by_title(title="cat-1")),
                list(Category.wise.filter_by_title(title="cat-2")),
                list(Category.wise.filter_by_title(title="cat-3")),
            ],
            [
                [category1],
                [category2],
                [category3],
            ]
        )

    @ not_more_than(0.09)
    def test_filter_orders_by_date_category_queryset(self):
        FilterOrdersByDateFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        category3 = Category.wise.get(title="cat-3")
        self.assertQuerysetEqual(
            list(
                Category.wise.filter_order_by_date(
                    end_date=datetime(2030, 1, 1))
            ),

            [
                category1,
                category2,
                category2,
                category2,
                category3,
                category2,
                category3,
                category1,

            ],
            ordered=False

        )

    @ not_more_than(0.09)
    def test_actives_category_queryset(self):
        ActivesFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        category3 = Category.wise.get(title="cat-3")
        self.assertQuerysetEqual(
            list(
                Category.wise.actives()
            ),

            [
                category1,
                category2,

            ],
            ordered=False

        )

    @ not_more_than(0.09)
    def test_deactives_category_queryset(self):
        DeactivesFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        self.assertQuerysetEqual(
            list(
                Category.wise.deactives()
            ),

            [
                category1,
                category2,

            ],
            ordered=False

        )

    @ not_more_than(0.09)
    def test_filter_order_by_city_category_queryset(self):
        FilterOrdersByCityFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        category3 = Category.wise.get(title="cat-3")

        self.assertQuerysetEqual(

            Category.wise.filter_order_by_city("city-1"),


            [
                category1,
                category2
            ],


            ordered=False
        )
        self.assertQuerysetEqual(

            Category.wise.filter_order_by_city("city-0"),

            [
                category2,
                category2,
                category3,
                category1
            ],
            ordered=False
        )

    @ not_more_than(0.09)
    def test_filter_orders_by_status_category_queryset(self):
        FilterOrdersByStatusFakeData.create_data()
        category1 = Category.wise.get(title="cat-1")
        category2 = Category.wise.get(title="cat-2")
        category3 = Category.wise.get(title="cat-3")

        self.assertQuerysetEqual(

            Category.wise.filter_order_by_statuses("W"),

            [category2],
            ordered=False
        )
        self.assertQuerysetEqual(

            Category.wise.filter_order_by_statuses("X"),

            list(),
            ordered=False
        )
        self.assertQuerysetEqual(

            Category.wise.filter_order_by_statuses("E"),

            list(),
            ordered=False
        )
        self.assertQuerysetEqual(

            Category.wise.filter_order_by_statuses("P"),

            [category1, category2, category3, category1],
            ordered=False
        )
        self.assertQuerysetEqual(

            Category.wise.filter_order_by_statuses("WSDE"),

            [category2, category2, category2, category3],
            ordered=False
        )
        self.assertQuerysetEqual(

            Category.wise.filter_order_by_statuses("WSD"),

            [category2, category2, category2, category3],
            ordered=False
        )
