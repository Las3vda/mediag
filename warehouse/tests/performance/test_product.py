from datetime import datetime

from django.test import TestCase
from django.contrib.auth import get_user_model

from painless.utils.decorators.time_decorators import not_more_than
from warehouse.models import (
    Product,

)
from warehouse.tests.test_data.product import (
    RangeStockFakeData,
    ActivesFakeData,
    DeactivesFakeData,
    FilterByPriceRangeFakeData,
    FilterBySkuFakeData,
    FilterOrdersByDateFakeData,
    GetBoughtUsersFakeData,
    GetUserBoughtCountFakeData,
    GetMaxSoldPackFakeData,
    GetMinSoldPackFakeData,
    GetRelatedBrandFakeData,
    GetRelatedCategoryFakeData,
    GetSoldPriceFakeData,
    GetSoldQuantityFakeData,
    HasDefaultRatingFakeData,
    OutOfStockFakeData,
    InStockFakeData,
    SpecialsFakeData,
)

User = get_user_model()


class ProductPerformanceTestCase(TestCase):

    @not_more_than(0.09)
    def test_range_stock_product_performance(self):
        RangeStockFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.range_stock(level="huge"))

    @not_more_than(0.09)
    def test_actives_product_performance(self):
        ActivesFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.actives())

    @not_more_than(0.09)
    def test_deactives_product_performance(self):
        DeactivesFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.deactives())

    @not_more_than(0.09)
    def test_in_stock_product_performance(self):
        InStockFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.in_stock())

    @not_more_than(0.09)
    def test_out_of_stock_product_performance(self):
        OutOfStockFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.out_of_stock())

    @not_more_than(0.09)
    def test_specials_product_performance(self):
        SpecialsFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.specials())

    @not_more_than(0.09)
    def test_has_default_rating_product_performance(self):
        HasDefaultRatingFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.has_default_rating())

    @not_more_than(0.09)
    def test_filter_by_sku_product_performance(self):
        FilterBySkuFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.filter_by_sku(sku="sku-1"))

    @not_more_than(0.09)
    def test_filter_by_price_range_product_performance(self):
        FilterByPriceRangeFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.filter_by_price_range(30000, 10000))

    @not_more_than(0.09)
    def test_get_sold_price_product_performance(self):
        GetSoldPriceFakeData.create_data()
        with self.assertNumQueries(1):
            products = Product.wise.get_sold_price()[3].sold_price

    @not_more_than(0.09)
    def test_get_sold_quantity_product_performance(self):
        GetSoldQuantityFakeData.create_data()
        with self.assertNumQueries(1):
            sold_price = Product.wise.get_sold_quantity()[3].sold_count

    @not_more_than(0.09)
    def test_get_user_bought_count_product_performance(self):
        GetUserBoughtCountFakeData.create_data()
        with self.assertNumQueries(1):
            sold_price = Product.wise.get_user_bought_count(
                distinct=True
            )[0].sold

    @not_more_than(0.09)
    def test_get_related_brand_product_performance(self):
        GetRelatedBrandFakeData.create_data()
        with self.assertNumQueries(1):
            brand = Product.wise.get_related_brand()[0].brand

    @not_more_than(0.09)
    def test_get_related_category_product_performance(self):
        GetRelatedCategoryFakeData.create_data()
        with self.assertNumQueries(1):
            category = Product.wise.get_related_category()[0].category

    # @not_more_than(0.09)
    # def test_get_related_orders_product_performance(self):
    #     GetRelatedOrdersFakeData.create_data()
    #     product1 = Product.wise.get(sku="sku-1")
    #     product2 = Product.wise.get(sku="sku-2")
    #     product3 = Product.wise.get(sku="sku-3")
    #     product4 = Product.wise.get(sku="sku-4")
    #     self.assertQuerysetEqual(
    #         [
    #             Product.wise.get_related_orders().all(),

    #         ],

    #         [

    #         ],
    #     )

    @not_more_than(0.09)
    def test_get_bought_users_product_performance(self):
        GetBoughtUsersFakeData.create_data()
        with self.assertNumQueries(1):
            products = list(Product.wise.get_bought_users().values_list(
                "packs__order_packs__order__user"))

    @not_more_than(0.09)
    def test_get_max_sold_pack_product_performance(self):
        GetMaxSoldPackFakeData.create_data()
        with self.assertNumQueries(1):
            max_sold_counts = list(Product.wise.get_max_sold_pack().values_list(
                "max_sold_count"))

    @not_more_than(0.09)
    def test_get_min_sold_pack_product_performance(self):
        GetMinSoldPackFakeData.create_data()
        with self.assertNumQueries(1):
            min_sold_counts = list(Product.wise.get_min_sold_pack().values_list(
                "min_sold_count"))

    @not_more_than(0.09)
    def test_filter_orders_by_date_product_performance(self):
        FilterOrdersByDateFakeData.create_data()
        with self.assertNumQueries(1):

            products = list(Product.wise.filter_orders_by_date(
                end_date=datetime(210, 1, 1))),
