from django.test import TestCase
from django.contrib.auth import get_user_model

from painless.utils.decorators.time_decorators import not_more_than
from warehouse.models import (
    Brand
)
from warehouse.tests.test_data.brand import (
    GetSoldCountFakeData,
    GetUserBoughtFakeData,
    GetUserBoughtCountFakeData,
    FilterByTitleFakeData,
    FilterOrdersByCityFakeData,
    FilterOrdersByDateFakeData,
    FilterOrdersByStatusFakeData,
)

User = get_user_model()


class BrandPerformanceTestCase(TestCase):

    @not_more_than(0.09)
    def test_get_user_bought_count_brand_performance(self):
        GetUserBoughtCountFakeData.create_data()
        with self.assertNumQueries(1):
            bought_count = Brand.wise.get_user_bought_count(distinct=True)[
                0].bought

    @not_more_than(0.09)
    def test_get_sold_quantity_brand_performance(self):
        GetSoldCountFakeData.create_data()
        with self.assertNumQueries(1):
            sold_quantity = Brand.wise.get_sold_count()[0].sold

    @not_more_than(0.09)
    def test_get_bought_users_brand_performance(self):
        GetUserBoughtFakeData.create_data()
        with self.assertNumQueries(1):
            users = Brand.wise.get_bought_users(distinct=True).values_list(
                "products__packs__order_packs__order__user__email")[0]
        with self.assertNumQueries(1):
            users = Brand.wise.get_bought_users(distinct=False).values_list(
                "products__packs__order_packs__order__user__email")[0]

    @not_more_than(0.09)
    def test_filter_by_title_brand_performance(self):
        FilterByTitleFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Brand.wise.filter_by_title(title="brand1"))

    @not_more_than(0.09)
    def test_filter_orders_by_date_brand_performance(self):
        FilterOrdersByDateFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Brand.wise.filter_order_by_date())

    @not_more_than(0.09)
    def test_filter_order_by_city_brand_performance(self):
        FilterOrdersByCityFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Brand.wise.filter_order_by_city("city-1"))

    @not_more_than(0.09)
    def test_filter_orders_by_status_brand_performance(self):
        FilterOrdersByStatusFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Brand.wise.filter_order_by_statuses("WSD"))
