from django.test import TestCase
from django.contrib.auth import get_user_model

from painless.utils.decorators.time_decorators import not_more_than
from warehouse.models import (
    Category
)
from warehouse.tests.test_data.category import (
    GetSoldCountFakeData,
    GetUserBoughtFakeData,
    GetUserBoughtCountFakeData,
    FilterByTitleFakeData,
    FilterOrdersByCityFakeData,
    FilterOrdersByDateFakeData,
    FilterOrdersByStatusFakeData,
    DeactivesFakeData,
    ActivesFakeData
)

User = get_user_model()


class CategoryPerformanceTestCase(TestCase):

    @not_more_than(0.09)
    def test_get_user_bought_count_category_performance(self):
        GetUserBoughtCountFakeData.create_data()
        with self.assertNumQueries(1):
            bought_count = Category.wise.get_user_bought_count(distinct=True)[
                0].bought

    @not_more_than(0.09)
    def test_get_sold_quantity_category_performance(self):
        GetSoldCountFakeData.create_data()
        with self.assertNumQueries(1):
            sold_quantity = Category.wise.get_sold_count()[0].sold

    @not_more_than(0.09)
    def test_get_bought_users_category_performance(self):
        GetUserBoughtFakeData.create_data()
        with self.assertNumQueries(1):
            users = Category.wise.get_bought_users(distinct=True).values_list(
                "products__packs__order_packs__order__user__email")[0]
        with self.assertNumQueries(1):
            users = Category.wise.get_bought_users(distinct=False).values_list(
                "products__packs__order_packs__order__user__email")[0]

    @not_more_than(0.09)
    def test_filter_by_title_category_performance(self):
        FilterByTitleFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Category.wise.filter_by_title(title="cat-1"))

    @not_more_than(0.09)
    def test_actives_category_performance(self):
        ActivesFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Category.wise.actives())

    @not_more_than(0.09)
    def test_deactives_category_performance(self):
        DeactivesFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Category.wise.deactives())

    @not_more_than(0.09)
    def test_filter_orders_by_date_category_performance(self):
        FilterOrdersByDateFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Category.wise.filter_order_by_date())

    @not_more_than(0.09)
    def test_filter_order_by_city_category_performance(self):
        FilterOrdersByCityFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Category.wise.filter_order_by_city("city-1"))

    @not_more_than(0.09)
    def test_filter_orders_by_status_category_performance(self):
        FilterOrdersByStatusFakeData.create_data()
        with self.assertNumQueries(1):
            categories = list(Category.wise.filter_order_by_statuses("WSD"))
